package com.example.snackz.project;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UpdateMenu extends AppCompatActivity {
    String MID;
    Bitmap bitmap;
    String Server=Sever.sever;
   // GalleryPhoto galleryPhoto;
    final int GALLERY_REQUEST = 22131;
   // String strPhoto;
    ImageView tIM;
    Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_menu);
        ShowMenu();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                tIM.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public void ShowMenu(){
        final TextView tName = (TextView) findViewById(R.id.Ename);
        final TextView tDetail = (TextView)findViewById(R.id.Edetail);
        final TextView tPrice1 = (TextView)findViewById(R.id.Eprice1);
        final TextView tPrice2 = (TextView)findViewById(R.id.Eprice2);
        final TextView tType = (TextView)findViewById(R.id.EType);
        tIM = (ImageView)findViewById(R.id.Eimage);


       // galleryPhoto = new GalleryPhoto(getApplicationContext());
        tIM.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
              //  startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                openGallery();
            }

        });


        Button btnSave = (Button) findViewById(R.id.SEMenu);
        Button btnCancel = (Button) findViewById(R.id.CEMenu);

        String url = Server+"ShowInforMenu.php";

        Intent intent= getIntent();
        MID = intent.getStringExtra("mid");
        System.out.println(MID+"ZZZZZZZZZZZZZZZZZZZZZZZZZ");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMid", MID));

        String resultServer  = getHttpPost(url,params);

        String strN = "";
        String strD = "";
        String strP = "";
        String strPic = "";
        String strT = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strN = c.getString("mname");
            strD = c.getString("detail");
            strP = c.getString("price");
            strPic = c.getString("picture");
            strT = c.getString("type");


            if(!strN.equals(""))
            {
                tName.setText(strN);
                tDetail.setText(strD);
                tPrice1.setText(strP);
                tType.setText(strT);
                bitmap=loadBitmap(Server+strPic);
                tIM.setImageBitmap(bitmap);
            }
            else
            {
                tName.setText("-");
                tDetail.setText("-");
                tPrice1.setText("-");
                tPrice2.setText("-");
                tType.setText("-");
                btnSave.setEnabled(false);
                btnCancel.requestFocus();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnSave = (Button) findViewById(R.id.SEMenu);
        // Perform action on click
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SaveData())
                {
                    Intent newActivity = new Intent(UpdateMenu.this,ShowInforMenu.class);
                    newActivity.putExtra("mid", MID);
                    startActivity(newActivity);

                }
            }
        });

        // btnCancel
        btnCancel = (Button) findViewById(R.id.CEMenu);
        // Perform action on click
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(UpdateMenu.this,ShowInforMenu.class);
                newActivity.putExtra("mid", MID);
                startActivity(newActivity);
            }
        });

    }

    public boolean SaveData() {
        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtNameM = (EditText) findViewById(R.id.Ename);
        final EditText txtDetalM = (EditText) findViewById(R.id.Edetail);
        final EditText txtPriceM = (EditText) findViewById(R.id.Eprice1);
        final EditText txtTypeM = (EditText) findViewById(R.id.EType);



        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);


        if (txtNameM.getText().length() == 0) {
            ad.setMessage("Please input [ชื่อเมนู] ");
            ad.show();
            txtNameM.requestFocus();
            return false;
        }
        if (txtDetalM.getText().length() == 0) {
            ad.setMessage("Please input [รายละเอียดเมนู] ");
            ad.show();
            txtDetalM.requestFocus();
            return false;
        }


        if (txtPriceM.getText().length() == 0) {
            ad.setMessage("Please input [ราคา] ");
            ad.show();
            txtPriceM.requestFocus();
            return false;
        }
        // Check Tel
        if (txtTypeM.getText().length() == 0) {
            ad.setMessage("Please input [ประเภท] ");
            ad.show();
            txtTypeM.requestFocus();
            return false;
        }

        String url = Server+"UpdateMenu.php";

        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameM", txtNameM.getText().toString()));
        params.add(new BasicNameValuePair("sDetailM", txtDetalM.getText().toString()));
        params.add(new BasicNameValuePair("sPriceM", txtPriceM.getText().toString()));
        params.add(new BasicNameValuePair("sTypeM", txtTypeM.getText().toString()));
        params.add(new BasicNameValuePair("sImage", encodeedImage));
        params.add(new BasicNameValuePair("sMid", MID.toString()));

        // System.out.print(encodeedImage+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        // System.out.println(ImageData+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(UpdateMenu.this, "AddMenu Successfully", Toast.LENGTH_SHORT).show();
            txtNameM.setText("");
            txtDetalM.setText("");
            txtPriceM.setText("");
            txtTypeM.setText("");
            Intent intent = new Intent(UpdateMenu.this, ShowInforMenu.class);
            intent.putExtra("mid", MID);
            startActivity(intent);
        }

        return true;
    }

    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }



}
