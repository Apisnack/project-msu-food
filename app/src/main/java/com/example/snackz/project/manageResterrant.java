package com.example.snackz.project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class manageResterrant extends AppCompatActivity {
    String MemberID;
    String RID;
    String strRid ="" ,strMID,strUID;
    private Bitmap bitmap;
    Button ButE;
    String Server=Sever.sever;
    Switch simpleSwitch;
    String strMemberID = "";

    ArrayList<HashMap<String, String>> MyArrList1;
    String[] Cmd = {"Report","Cancel"};
    final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_resterrant);


        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefreshMenu);
        pullToRefresh.setOnRefreshListener(new android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ShowMenu();
                saveAVGrating();
                showReview();
                showRating();
                pullToRefresh.setRefreshing(false);
            }
        });


        showRestterrant();
        showUser();
        ShowMenu();
        showReview();
        showRating();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // initiate a Switch
        simpleSwitch = (Switch) findViewById(R.id.switch1);
       // simpleSwitch.setChecked(true);
        simpleSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check current state of a Switch (true or false).
                Boolean switchState = simpleSwitch.isChecked();

                if (switchState == true){
                    Toast.makeText( manageResterrant.this, "On", Toast.LENGTH_SHORT).show();
                    String status = "1";
                    upDateStatusRes(status);
                }
                else if (switchState == false){
                    Toast.makeText( manageResterrant.this, "Off", Toast.LENGTH_SHORT).show();
                    String status = "0";
                    upDateStatusRes(status);
                }
            }
        });


    }





    public void showRestterrant() {
        final Switch aSwitch = (Switch)findViewById(R.id.switch1);
        final TextView tRN = (TextView)findViewById(R.id.trname);
        final TextView tDe = (TextView)findViewById(R.id.tde);
        final TextView tMin = (TextView)findViewById(R.id.tmin);
        final TextView tTime = (TextView)findViewById(R.id.tT);
        final TextView tAdd = (TextView)findViewById(R.id.tadd);

        String url = Server+"manageRestterrant.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", strMemberID));

        String resultServer  = getHttpPost(url,params);

        String strRname = "";
        String strDetail = "";
        String strMin = "";
        String strTime = "";
        String strRAddress = "";
        String strStatus = "";
        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strRid = c.getString("sRid");
            strRname = c.getString("RName");
            strDetail = c.getString("Detail");
            strMin = c.getString("MinOrder");
            strTime = c.getString("Time");
            strRAddress = c.getString("RAddress");
            strStatus = c.getString("status");
         //   System.out.println(strRid+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            if(!strRname.equals(""))
            {
                tRN.setText(strRname);
                tDe.setText(strDetail);
                tMin.setText(strMin);
                tTime.setText(strTime);
                tAdd.setText(strRAddress);
                if (strStatus.equals("1")){
                    aSwitch.setChecked(true);
                }
                else if (strStatus.equals("0")){
                    aSwitch.setChecked(false);
                }
                // tRN.setText(strRname);

            }
            else
            {
                tRN.setText("-");
                tDe.setText("-");
                tMin.setText("-");
                tTime.setText("-");
                tAdd.setText("-");
                // tRN.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void saveAVGrating() {
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"AVGrating.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", strRid));
        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(manageResterrant.this, "Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean upDateStatusRes(String status) {


        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"upDateStatus.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid",strRid));
        params.add(new BasicNameValuePair("sStatus",status));



        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
            Toast.makeText(manageResterrant.this, "Update Success", Toast.LENGTH_SHORT).show();
        }


        return true;
    }


    public void showUser() {

        final TextView tUname = (TextView) findViewById(R.id.t1);
        final ImageView iM = (ImageView) findViewById(R.id.PhotoProfile2);

        Intent intent = getIntent();
        MemberID = intent.getStringExtra("MemberID");
        //System.out.println(MemberID + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server+"getByMemberID.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", strMemberID));


        String resultServer = getHttpPost(url, params);

        String strUID = "";
        String strPhoto = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strUID = c.getString("Name");
            strPhoto = c.getString("Picture");
            //System.out.println(strPhoto + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            // System.out.println(strUID + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            if (!strUID.equals("")) {
                tUname.setText(strUID);
                bitmap = loadBitmap(Server + strPhoto);
                iM.setImageBitmap(bitmap);

            } else {
                tUname.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void ShowMenu(){
        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listViewR);


        String url = Server+"ShowMenu.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid", strRid));
        //System.out.println(strRid+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("mid", c.getString("mid"));
                map.put("mname", c.getString("mname"));
                map.put("detail", c.getString("detail"));
                map.put("price", c.getString("price"));
                map.put("picture", c.getString("picture"));
                map.put("type", c.getString("type"));
                map.put("rid", c.getString("rid"));
                MyArrList.add(map);

            }

            lisView1.setAdapter(new ImageAdapter(this,MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    strMID = MyArrList.get(position).get("mid").toString();

                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(manageResterrant.this, ShowInforMenu.class);
                    newActivity.putExtra("mid", strMID);
                    startActivity(newActivity);

                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public class ImageAdapter extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.culumn_resterrant, null);
            }

            // ColImage
            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPathR);
            imageView.getLayoutParams().height = 150;
            imageView.getLayoutParams().width = 150;
            String strPhoto;
            try
            {

                strPhoto = MyArr.get(position).get("picture");
                System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                bitmap=loadBitmap(Server+strPhoto);
                imageView.setImageBitmap(bitmap);

            } catch (Exception e) {
                // When Error
                imageView.setImageResource(android.R.drawable.ic_menu_report_image);
            }
            // ColPosition
           // String url = "https://foodmsu.000webhostapp.com/Foodmsu/getByMemberID.php";


            // ColPicname
            TextView txtPicNameM = (TextView) convertView.findViewById(R.id.ColNameR);
            txtPicNameM.setPadding(50, 0, 0, 0);
            txtPicNameM.setText( MyArr.get(position).get("mname"));

            TextView txtDetailM = (TextView) convertView.findViewById(R.id.ColDetailR);
            txtDetailM.setPadding(50, 0, 0, 0);
            txtDetailM.setText(MyArr.get(position).get("detail"));

            TextView txtPriceM = (TextView) convertView.findViewById(R.id.ColPriceR);
            txtPriceM.setPadding(50, 0, 0, 0);
            txtPriceM.setText( MyArr.get(position).get("price"));

            TextView txtTypeM = (TextView) convertView.findViewById(R.id.ColTypeR);
            txtTypeM.setPadding(50, 0, 0, 0);
            txtTypeM.setText(MyArr.get(position).get("type"));

            return convertView;

        }

    }

    public void showReview(){
        // listView1
        final ListView lisViewCommentManage = (ListView) findViewById(R.id.listCommentManage);


        String url = Server + "ShowReview.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", strRid));
        //System.out.println(RID+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));



            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("rv_id", c.getString("rv_id"));
                map.put("comment", c.getString("comment"));
                map.put("rating", c.getString("rating"));
                map.put("rid", c.getString("rid"));
                map.put("uid", c.getString("uid"));
                MyArrList.add(map);

            }

            lisViewCommentManage.setAdapter(new ImageAdapter2(this, MyArrList));
            registerForContextMenu(lisViewCommentManage);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //menu.setHeaderIcon(android.R.drawable.btn_star_big_on);
        menu.setHeaderTitle("Command");
        String[] menuItems = Cmd;
        for (int i = 0; i<menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = Cmd;
        String CmdName = menuItems[menuItemIndex];

        // Check Event Command
        if ("Cancel".equals(CmdName)) {
            Toast.makeText(manageResterrant.this,"Your Selected Cancel",Toast.LENGTH_LONG).show();
        }

        else if ("Report".equals(CmdName)) {
            Toast.makeText(manageResterrant.this, "Your Selected Report", Toast.LENGTH_LONG).show();

            String Comment = MyArrList.get(info.position).get("comment").toString();
            String RVID = MyArrList.get(info.position).get("rv_id").toString();
            String UID = MyArrList.get(info.position).get("uid").toString();

            Intent newActivity = new Intent(manageResterrant.this, Report.class);
            newActivity.putExtra("sComment", Comment);
            newActivity.putExtra("sRvid", RVID);
            newActivity.putExtra("sUID", UID);
            startActivity(newActivity);
        }
        return true;
    }
    public void showRating() {
        // textView1
        final TextView txtView1 = (TextView) findViewById(R.id.textViewRatingManage);
        // seekBar1
        final RatingBar rating_Bar = (RatingBar) findViewById(R.id.ratingCommentManage);

        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        //System.out.println(RID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server + "getRestterrant.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", strRid));

        String resultServer = getHttpPost(url, params);

        // String strUID = "";
        String strRating = "";




        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            //  strUID = c.getString("Name");
            strRating = c.getString("Rating");
            if (!strRating.equals("")) {
                rating_Bar.setEnabled(false);
                rating_Bar.setMax(5);
                rating_Bar.setRating(Float.valueOf(strRating));
                txtView1.setText(strRating);
            } else {


            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        // Button1
       /* final Button btn1 = (Button) findViewById(R.id.vote);
        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowDialog();
            }
        });*/
    }

    public class ImageAdapter2 extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter2(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_review_manage, null);
            }

            // ColImage

            String UID = MyArr.get(position).get("uid");

            String url = Server+"getByMemberID.php";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sMemberID", UID));


            String resultServer = getHttpPost(url, params);

            String strUID = "";

            TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameReviewManage);
            JSONObject c;
            try {
                c = new JSONObject(resultServer);
                strUID = c.getString("Name");

                if (!strUID.equals("")) {
                    txtPicName.setPadding(5, 0, 0, 0);
                    txtPicName.setText(strUID+" :");

                } else {
                    txtPicName.setText("-");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // ColPicname
            TextView txtPicNameM = (TextView) convertView.findViewById(R.id.ColCommentManage);
            txtPicNameM.setPadding(5, 0, 0, 0);
            txtPicNameM.setText(MyArr.get(position).get("comment"));

            // ColratingBar
          /*  RatingBar Rating = (RatingBar) convertView.findViewById(R.id.ratingReviewManage);
            Rating.setPadding(10, 0, 0, 0);
            Rating.setEnabled(false);
            Rating.setMax(5);
            Rating.setRating(Float.valueOf(MyArr.get(position).get("rating").toString()));
*/

            return convertView;

        }

    }


    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public String getJSONUrl(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download file..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public void EditResterrantB(View view) {
        //Button btnLogin = (Button) findViewById(R.id.InsertM);
        Intent newActivity = new Intent(manageResterrant.this, EditResterrant.class);
        newActivity.putExtra("RID", strRid);
        startActivity(newActivity);

    }

    public void AddMenu(View view) {
        //Button btnLogin = (Button) findViewById(R.id.InsertM);
        Intent newActivity = new Intent(manageResterrant.this, AddMenu.class);
        newActivity.putExtra("RID", strRid);
        newActivity.putExtra("MemberID", MemberID);
        startActivity(newActivity);
    }

    public void ShowOrder(View view){
        Intent newActivity = new Intent(manageResterrant.this, ShowOrder.class);
        newActivity.putExtra("RID", strRid);
        startActivity(newActivity);
    }
}
