package com.example.snackz.project;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.ImageBase64;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
public class UpdateActivity extends AppCompatActivity {
    String Server=Sever.sever;
    ImageView selectphoto;
    private Bitmap bitmap;
    Uri imgUri;
    String strMemberID;
    private static final int GALLERY_REQUEST = 22131;
    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);
        //*** Get Member ID from Session
         strMemberID = usrHelper.getMemberID();


        showInfo();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        selectphoto = (ImageView) findViewById(R.id.UpdateProfile);
        selectphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                openGallery();
            }

        });
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                selectphoto.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void showInfo()
    {
        // txtMemberID,txtUsername,txtPassword,txtConPassword,txtName,txtEmail,txtTel
        final TextView tE = (TextView)findViewById(R.id.txtE);
        final TextView tP = (TextView)findViewById(R.id.txtP);
        final TextView tCP = (TextView)findViewById(R.id.txtConP);
        final TextView tN = (TextView)findViewById(R.id.txtN);
        final TextView tA = (TextView)findViewById(R.id.txtAdd);
        final TextView tTel = (TextView)findViewById(R.id.txtT);
        final ImageView iM = (ImageView) findViewById(R.id.UpdateProfile) ;

        Button btnSave = (Button) findViewById(R.id.btnSave);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);

        String url = Server+"getByMemberID.php";

        Intent intent= getIntent();
        final String MemberID = intent.getStringExtra("MemberID");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", MemberID));



        String resultServer  = getHttpPost(url,params);

        String strUID = "";
        String strEmail = "";
        String strName = "";
        String strPass = "";
        String strPhone = "";
        String strAddress = "";
        String strPhoto = "";
        String strTypeUser ="";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strUID = c.getString("UID");
            strEmail = c.getString("Email");
            strPass = c.getString("Password");
            strName = c.getString("Name");
            strPhone = c.getString("Phone");
            strAddress = c.getString("Address");
            strPhoto = c.getString("Picture");
            strTypeUser = c.getString("Type");
            if(!strUID.equals(""))
            {
                if (strTypeUser.equals("Facebook")) {
                    tE.setText(strEmail);
                    tP.setText(strPass);
                    tCP.setText(strPass);
                    tN.setText(strName);
                    tP.setEnabled(false);
                    tCP.setEnabled(false);
                    tTel.setText(strPhone);
                    tA.setText(strAddress);
                    bitmap=loadBitmap(Server+strPhoto);
                    iM.setImageBitmap(bitmap);
                }else{
                    tE.setText(strEmail);
                    tN.setText(strName);
                    tP.setText(strPass);
                    tCP.setText(strPass);
                    tTel.setText(strPhone);
                    tA.setText(strAddress);
                    bitmap=loadBitmap(Server+strPhoto);
                    iM.setImageBitmap(bitmap);
                }
            }
            else
            {

                tE.setText("-");
                tP.setText("-");
                tCP.setText("-");
                tN.setText("-");
                tTel.setText("-");
                tA.setText(strAddress);
                btnSave.setEnabled(false);
                btnCancel.requestFocus();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnSave = (Button) findViewById(R.id.btnSave);
        // Perform action on click
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SaveData())
                {
                    Intent newActivity = new Intent(UpdateActivity.this,Home.class);
                    newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    newActivity.putExtra("MemberID", MemberID);
                    startActivity(newActivity);

                }
            }
        });

        // btnCancel
        btnCancel = (Button) findViewById(R.id.btnCancel);
        // Perform action on click
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(UpdateActivity.this,Home.class);
                newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                newActivity.putExtra("MemberID", MemberID);
                startActivity(newActivity);
            }
        });

    }

    public boolean SaveData()
    {
        // txtMemberID,txtPassword,txtName,txtEmail,txtTel
        final TextView tE = (TextView)findViewById(R.id.txtE);
        final TextView tP = (TextView)findViewById(R.id.txtP);
        final TextView tCP = (TextView)findViewById(R.id.txtConP);
        final TextView tN = (TextView)findViewById(R.id.txtN);
        final TextView tA = (TextView)findViewById(R.id.txtAdd);
        final TextView tTel = (TextView)findViewById(R.id.txtT);


        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Password
        if(tP.getText().length() == 0 || tCP.getText().length() == 0 )
        {
            ad.setMessage("Please input [Password/Confirm Password] ");
            ad.show();
            tP.requestFocus();
            return false;
        }
        // Check Password and Confirm Password (Match)
        if(!tP.getText().toString().equals(tCP.getText().toString()))
        {
            ad.setMessage("Password and Confirm Password Not Match! ");
            ad.show();
            tCP.requestFocus();
            return false;
        }
        // Check Name
        if(tN.getText().length() == 0)
        {
            ad.setMessage("Please input [Name] ");
            ad.show();
            tN.requestFocus();
            return false;
        }
        // Check Email
        if(tA.getText().length() == 0)
        {
            ad.setMessage("Please input [Address] ");
            ad.show();
            tA.requestFocus();
            return false;
        }
        // Check Tel
        if(tTel.getText().length() == 0)
        {
            ad.setMessage("Please input [Phone] ");
            ad.show();
            tTel.requestFocus();
            return false;
        }

        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);

        String url = Server+"updateData.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID",strMemberID));
        params.add(new BasicNameValuePair("sPassword", tP.getText().toString()));
        params.add(new BasicNameValuePair("sName", tN.getText().toString()));
        params.add(new BasicNameValuePair("sAddress", tA.getText().toString()));
        params.add(new BasicNameValuePair("sTel", tTel.getText().toString()));
        params.add(new BasicNameValuePair("sImage",encodeedImage));

        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
            Toast.makeText(UpdateActivity.this, "Update Data Successfully", Toast.LENGTH_SHORT).show();
        }


        return true;
    }

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }
}
