package com.example.snackz.project;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class New extends AppCompatActivity {
    String MemberID;
    private final String TAG = this.getClass().getName();
    ImageView selectphoto;
   // GalleryPhoto galleryPhoto;
   // String strPhoto;
    final int GALLERY_REQUEST = 22131;
    String Server=Sever.sever;
    Uri imgUri;
    Bitmap bitmap;
    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

       // showInfo();
        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Intent intent = getIntent();
        MemberID = intent.getStringExtra("MemberID");


        Button Save ;
        Button Cancel ;

        Save = (Button) findViewById(R.id.Save1);
        // Perform action on click
        Save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SaveData()) {

                }
            }
        });

        // btnCancel
        Cancel = (Button) findViewById(R.id.Cal1);
        // Perform action on click
        Cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(New.this,Home.class);
                newActivity.putExtra("MemberID", MemberID);
                startActivity(newActivity);
            }
        });

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

       // galleryPhoto = new GalleryPhoto(getApplicationContext());
        selectphoto = (ImageView) findViewById(R.id.ResterrantP);
        selectphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                openGallery();
            }

        });

    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                selectphoto.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
   /* public void showInfo()
    {
        // txtMemberID,txtMemberID,txtUsername,txtPassword,txtName,txtEmail,txtTel
        final TextView tU = (TextView)findViewById(R.id.tU1);



        String url = "http://192.168.56.1/loginpro/getByMemberID.php";

        Intent intent= getIntent();
         MemberID = intent.getStringExtra("MemberID");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", MemberID));

        /** Get result from Server (Return the JSON Code)
         *
         * {"MemberID":"2","Username":"adisorn","Password":"adisorn@2","Name":"Adisorn Bunsong","Tel":"021978032","Email":"adisorn@thaicreate.com"}
         */

      /*  String resultServer  = getHttpPost(url,params);

        String strUID = "";


        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strUID = c.getString("UID");


            if(!strUID.equals(""))
            {
               tU.setText(strUID);

            }
            else
            {
               tU.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }*/
    public boolean SaveData()
    {
        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtNameStore = (EditText)findViewById(R.id.txtNameStore);
        final EditText txtDetail = (EditText)findViewById(R.id.txtDetail);
        final EditText txtTime = (EditText)findViewById(R.id.txtTime);
        final EditText txtPrice = (EditText)findViewById(R.id.txtPrice);
        final EditText txtMinOrder = (EditText)findViewById(R.id.txtMinOrder);
        final EditText txtAddress = (EditText)findViewById(R.id.txtAddress);
        //final TextView tUid = (TextView) findViewById(R.id.tU1);

        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Username
        if(txtNameStore.getText().length() == 0)
        {
            ad.setMessage("Please input [ชื่อร้าน] ");
            ad.show();
            txtNameStore.requestFocus();
            return false;
        }
        // Check Password
        if(txtDetail.getText().length() == 0 || txtDetail.getText().length() == 0 )
        {
            ad.setMessage("Please input [รายละเอียดร้าน] ");
            ad.show();
            txtDetail.requestFocus();
            return false;
        }
        // Check Password and Confirm Password (Match)
        // Check Name
        if(txtTime.getText().length() == 0)
        {
            ad.setMessage("Please input [เวลา เปิด/ปิด] ");
            ad.show();
            txtTime.requestFocus();
            return false;
        }
        // Check Email
        if(txtPrice.getText().length() == 0)
        {
            ad.setMessage("Please input [ค่าส่ง] ");
            ad.show();
            txtPrice.requestFocus();
            return false;
        }
        // Check Tel
        if(txtMinOrder.getText().length() == 0)
        {
            ad.setMessage("Please input [MinOrder] ");
            ad.show();
            txtMinOrder.requestFocus();
            return false;
        }

        if(txtAddress.getText().length() == 0)
        {
            ad.setMessage("Please input [ที่อยู่] ");
            ad.show();
            txtAddress.requestFocus();
            return false;
        }



        String url = Server+"saveADDDataStore.php";

        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameStore", txtNameStore.getText().toString()));
        params.add(new BasicNameValuePair("sDetail", txtDetail.getText().toString()));
        params.add(new BasicNameValuePair("sTime", txtTime.getText().toString()));
        params.add(new BasicNameValuePair("sPrice", txtPrice.getText().toString()));
        params.add(new BasicNameValuePair("sMinOrder", txtMinOrder.getText().toString()));
        params.add(new BasicNameValuePair("sAddress", txtAddress.getText().toString()));
        params.add(new BasicNameValuePair("sUID", MemberID));
        params.add(new BasicNameValuePair("sImage", encodeedImage));

        String resultServer  = getHttpPost(url,params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
        }
        else
        {
            Toast.makeText(New.this, "Save Data Successfully", Toast.LENGTH_SHORT).show();
            txtNameStore.setText("");
            txtDetail.setText("");
            txtTime.setText("");
            txtPrice.setText("");
            txtMinOrder.setText("");
            txtAddress.setText("");

            Intent newActivity = new Intent(New.this,Home.class);
            newActivity.putExtra("MemberID", MemberID);
            startActivity(newActivity);
        }


        return true;
    }

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }
}
