package com.example.snackz.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ShowInforOrder extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String Server=Sever.sever;
    String OID;
    String strLat = "";
    String strLong = "";
    String strUser = "";
    String strRID = "";
    Button ok;
    Button cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_infor_order);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        Intent intent= getIntent();
        OID = intent.getStringExtra("oid");

        Showdata();
        ShowNameUser();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

         ok = (Button)findViewById(R.id.Bok);
         cancel = (Button)findViewById(R.id.Bcal);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strMessed = "รายการสั่งซื้อของคุณได้รับการยืนยัน";
                String Status ="1";
                upDateOrder(Status);
                sendNotificationToUser(strMessed);
            }
        });




        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strMessed = "รายการสั่งซื้อของคุณได้ถูกยกเลิก";
                String Status ="2";
                upDateOrder(Status);
                sendNotificationToUser(strMessed);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void Showdata() {
        final TextView OName = (TextView) findViewById(R.id.orderM);
        final TextView OPrice = (TextView) findViewById(R.id.orderP);
        final TextView OPhone = (TextView) findViewById(R.id.orderT);
        final TextView OAddress = (TextView) findViewById(R.id.orderA);
        final Button ok1 = (Button)findViewById(R.id.Bok);
        final Button cancel2 = (Button)findViewById(R.id.Bcal);
        final TextView tRemark = (TextView) findViewById(R.id.RemarkOrder);
        final TextView tDateTime = (TextView) findViewById(R.id.DateTimeInforOrder);


        //System.out.println(MemberID + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server+"ShowInforOrder.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sOID", OID));

        String resultServer = getHttpPost(url, params);

        String strOID = "";
        String strName = "";
        String strPrice = "";
        String strPhone = "";
        String strAddress= "";
        String Status = "";
        String Remark = "";
        String Datetime = "";


        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strOID = c.getString("oid");
            strName = c.getString("orderlist");
            strPrice = c.getString("price");
            strPhone = c.getString("phone");
            strAddress = c.getString("address");
            strLat = c.getString("latitude");
            strLong = c.getString("longtitude");
            strRID = c.getString("rid");
            strUser = c.getString("uid");
            Status = c.getString("status");
            Remark = c.getString("remark");
            Datetime = c.getString("datetime");

            if (!strOID.equals("")) {
                OName.setText(strName);
                OPrice.setText(strPrice);
                OPhone.setText(strPhone);
                OAddress.setText(strAddress);
                tRemark.setText(Remark);
                tDateTime.setText(Datetime);


                if (Status.equals("1")){
                    ok1.setEnabled(false);
                    cancel2.setEnabled(false);
                }
                else if (Status.equals("2")){
                    ok1.setEnabled(false);
                    cancel2.setEnabled(false);
                }
            } else {
                OName.setText("-");
                OPrice.setText("-");
                OPhone.setText("-");
                OAddress.setText("-");
                //OUser.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void ShowNameUser(){

        final TextView OUser = (TextView) findViewById(R.id.orderU);

        String url = Server + "getByMemberID.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", strUser));


        String resultServer = getHttpPost(url, params);

        // String strUID = "";
        String strNameUser = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strNameUser = c.getString("Name");
            if (!strNameUser.equals("")) {
                //  tUname.setText(strUID);
                OUser.setText(strNameUser);

            } else {
                //   tUname.setText("-");
                OUser.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void sendNotificationToUser(String messag) {
        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"send_noti_User.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sUID",strUser));
        params.add(new BasicNameValuePair("sText",messag));
        Log.e("UID",strUser);
        Log.e("MESSED",messag);
        getHttpPost(url,params);
        Toast.makeText(this, "Send Notification", Toast.LENGTH_SHORT).show();
    }


    public boolean upDateOrder(String status) {
        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"upDateStatusOID.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sOID",OID));
        params.add(new BasicNameValuePair("sStatus", status));


        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
            Toast.makeText(ShowInforOrder.this, "Successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ShowInforOrder.this, ShowOrder.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
            intent.putExtra("RID", strRID);
            startActivity(intent);
        }


        return true;
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Double Latti = Double.parseDouble(strLat);
        Double Longti = Double.parseDouble(strLong);
        // Add a marker in Sydney and move the camera
        //LatLng Location = new LatLng(Latti, Longti);
        //mMap.addMarker(new MarkerOptions().position(Location).title("Marker in Location"));
       // mMap.moveCamera(CameraUpdateFactory.newLatLng(Location));
        goTolocationZoom(Latti,Longti,15);
    }

    private void goTolocationZoom(double lat ,double lng ,float zoom){
        LatLng ll = new LatLng(lat,lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mMap.addMarker(new MarkerOptions().position(ll).title("My Location"));
        mMap.moveCamera(update);
    }



    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
