package com.example.snackz.project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowInforMenu extends AppCompatActivity {
    String MID;
    Bitmap bitmap;
    ImageView txtImage;
    String strMid;
    String Server=Sever.sever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_infor_menu);

        ShowInforMenu();
        //ShowSpeMenu();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
    public void ShowInforMenu(){

        txtImage = (ImageView)findViewById(R.id.IF1);
        final TextView txtName = (TextView)findViewById(R.id.IF2);
        final TextView txtDetail = (TextView)findViewById(R.id.IF3);
        final TextView txtType = (TextView)findViewById(R.id.IF4);
        final TextView txtPrice = (TextView)findViewById(R.id.IF5);
        final TextView txtPrice2 = (TextView)findViewById(R.id.IF6);

        Intent intent = getIntent();
        MID = intent.getStringExtra("mid");
        System.out.println(MID+"Showwwwwwwwwwwwwwwwwwwwwwwwwwwwww");

        String url = Server+"ShowInforMenu.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMid", MID));


        String resultServer = getHttpPost(url, params);
        strMid = "";
        String strName = "";
        String strDetail = "";
        String strType = "";
        String strPrice = "";
        String strPhoto = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strMid = c.getString("mid");
            strName = c.getString("mname");
            strDetail = c.getString("detail");
            strType = c.getString("type");
            strPrice = c.getString("price");
            strPhoto = c.getString("picture");
            //System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            if (!strName.equals("")) {
                txtName.setText(strName);
                txtDetail.setText(strDetail);
                txtType.setText(strType);
                txtPrice.setText(strPrice);
                bitmap=loadBitmap(Server+strPhoto);
                txtImage.setImageBitmap(bitmap);

            } else {
                txtName.setText("-");
                txtDetail.setText("-");
                txtType.setText("-");
                txtPrice.setText("-");
                txtPrice2.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }

   /* public void ShowSpeMenu(){
        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listMenuSpe);


        String url = Server+"ShowSpeMenuList.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMid", MID));
        //System.out.println(strRid+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("sid", c.getString("sid"));
                map.put("sname", c.getString("sname"));
                map.put("price", c.getString("price"));
                map.put("mid", c.getString("mid"));

                MyArrList.add(map);

            }

            lisView1.setAdapter(new ShowInforMenu.ImageAdapter(this,MyArrList));

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }*/


    public class ImageAdapter extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_spe_menu, null);
            }


            TextView txtPriceM = (TextView) convertView.findViewById(R.id.SpeMenu);
            txtPriceM.setPadding(50, 0, 0, 0);
            txtPriceM.setText( MyArr.get(position).get("sname"));

            TextView txtTypeM = (TextView) convertView.findViewById(R.id.SpePrice);
            txtTypeM.setPadding(50, 0, 0, 0);
            txtTypeM.setText(MyArr.get(position).get("price"));

            return convertView;

        }

    }



    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

  /*  public String getJSONUrl(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download file..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }*/


    public void UpdateMenu(View view) {
        Intent newActivity = new Intent(ShowInforMenu.this, UpdateMenu.class);
        newActivity.putExtra("mid", strMid);
        startActivity(newActivity);
    }

  /*  public void SaveAddSpeMenu(View view){
        final EditText txtNameSpe = (EditText)findViewById(R.id.txtNameSpe);
        final EditText txtPriceSpe = (EditText)findViewById(R.id.txtPriceSpe);

        Intent intent = getIntent();
        MID = intent.getStringExtra("mid");
        System.out.println(MID+"Saveeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");


        String url = Server+"saveSpeMenu.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameSpe", txtNameSpe.getText().toString()));
        params.add(new BasicNameValuePair("sPriceSpe", txtPriceSpe.getText().toString()));
        params.add(new BasicNameValuePair("sMid", MID));

        String resultServer  = getHttpPost(url,params);

        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
        }
        else
        {
            Toast.makeText(ShowInforMenu.this, "Save Data Successfully", Toast.LENGTH_SHORT).show();
            txtNameSpe.setText("");
            txtPriceSpe.setText("");
            Intent newActivity = new Intent(ShowInforMenu.this,ShowInforMenu.class);
            newActivity.putExtra("mid", MID);
            startActivity(newActivity);
        }


    }
*/
}
