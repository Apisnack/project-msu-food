package com.example.snackz.project;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.ImageBase64;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EditResterrant extends AppCompatActivity {
    String Server=Sever.sever;
    ImageView selectphoto;
    private Bitmap bitmap;
    Uri imgUri;
    String RID;
    String strMemberID;
    private static final int GALLERY_REQUEST = 22131;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_resterrant);

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();

        showInfo();

        selectphoto = (ImageView) findViewById(R.id.UpdateEdit);
        selectphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                openGallery();
            }

        });
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                selectphoto.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }



    public void showInfo()
    {
         Intent intent= getIntent();
         RID = intent.getStringExtra("RID");
        // txtMemberID,txtUsername,txtPassword,txtConPassword,txtName,txtEmail,txtTel

        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtNameStore = (EditText)findViewById(R.id.txtNameStoreEdit);
        final EditText txtDetail = (EditText)findViewById(R.id.txtDetailEdit);
        final EditText txtTime = (EditText)findViewById(R.id.txtTimeEdit);
        final EditText txtPrice = (EditText)findViewById(R.id.txtPriceEdit);
        final EditText txtMinOrder = (EditText)findViewById(R.id.txtMinOrderEdit);
        final EditText txtAddress = (EditText)findViewById(R.id.txtAddressEdit);
        final ImageView iM = (ImageView) findViewById(R.id.UpdateEdit) ;

        Button btnSave = (Button) findViewById(R.id.btnSave);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);

        String url = Server+"manageRestterrant.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", strMemberID));

        String resultServer  = getHttpPost(url,params);

        String strRname = "";
        String strDetail = "";
        String strMin = "";
        String strTime = "";
        String strRAddress = "";
        String strRid = "";
        String cost = "";
        String strPhoto ="";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strRid = c.getString("sRid");
            strRname = c.getString("RName");
            strDetail = c.getString("Detail");
            cost = c.optString("Cost");
            strMin = c.getString("MinOrder");
            strTime = c.getString("Time");
            strRAddress = c.getString("RAddress");
            strPhoto = c.getString("Rpicture");
            //   System.out.println(strRid+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            if(!strRname.equals(""))
            {
                txtNameStore.setText(strRname);
                txtDetail.setText(strDetail);
                txtMinOrder.setText(strMin);
                txtTime.setText(strTime);
                txtPrice.setText(cost);
                txtAddress.setText(strRAddress);
                bitmap=loadBitmap(Server+strPhoto);
                iM.setImageBitmap(bitmap);

            }
            else
            {
                txtNameStore.setText("-");
                txtDetail.setText("-");
                txtMinOrder.setText("-");
                txtTime.setText("-");
                txtAddress.setText("-");
                txtPrice.setText("-");
                // tRN.setText("-");

            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        btnSave = (Button) findViewById(R.id.SaveEditResterrant);
        // Perform action on click
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SaveData())
                {
                    Intent newActivity = new Intent(EditResterrant.this,manageResterrant.class);
                    newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    newActivity.putExtra("MemberID", strMemberID);
                    startActivity(newActivity);

                }
            }
        });

        // btnCancel
        btnCancel = (Button) findViewById(R.id.Cal1EditResterrant);
        // Perform action on click
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(EditResterrant.this,manageResterrant.class);
                newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
                newActivity.putExtra("MemberID", strMemberID);
                startActivity(newActivity);
            }
        });

    }



    public boolean SaveData() {
        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtNameStore = (EditText)findViewById(R.id.txtNameStoreEdit);
        final EditText txtDetail = (EditText)findViewById(R.id.txtDetailEdit);
        final EditText txtTime = (EditText)findViewById(R.id.txtTimeEdit);
        final EditText txtPrice = (EditText)findViewById(R.id.txtPriceEdit);
        final EditText txtMinOrder = (EditText)findViewById(R.id.txtMinOrderEdit);
        final EditText txtAddress = (EditText)findViewById(R.id.txtAddressEdit);
        //final TextView tUid = (TextView) findViewById(R.id.tU1);

        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        Intent intent= getIntent();
        RID = intent.getStringExtra("RID");

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Username
        if(txtNameStore.getText().length() == 0)
        {
            ad.setMessage("Please input [ชื่อร้าน] ");
            ad.show();
            txtNameStore.requestFocus();
            return false;
        }
        // Check Password
        if(txtDetail.getText().length() == 0 || txtDetail.getText().length() == 0 )
        {
            ad.setMessage("Please input [รายละเอียดร้าน] ");
            ad.show();
            txtDetail.requestFocus();
            return false;
        }
        // Check Password and Confirm Password (Match)
        // Check Name
        if(txtTime.getText().length() == 0)
        {
            ad.setMessage("Please input [เวลา เปิด/ปิด] ");
            ad.show();
            txtTime.requestFocus();
            return false;
        }
        // Check Email
        if(txtPrice.getText().length() == 0)
        {
            ad.setMessage("Please input [ค่าส่ง] ");
            ad.show();
            txtPrice.requestFocus();
            return false;
        }
        // Check Tel
        if(txtMinOrder.getText().length() == 0)
        {
            ad.setMessage("Please input [MinOrder] ");
            ad.show();
            txtMinOrder.requestFocus();
            return false;
        }

        if(txtAddress.getText().length() == 0)
        {
            ad.setMessage("Please input [ที่อยู่] ");
            ad.show();
            txtAddress.requestFocus();
            return false;
        }



        String url = Server+"UpdateDataStore.php";

        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameStore", txtNameStore.getText().toString()));
        params.add(new BasicNameValuePair("sDetail", txtDetail.getText().toString()));
        params.add(new BasicNameValuePair("sTime", txtTime.getText().toString()));
        params.add(new BasicNameValuePair("sPrice", txtPrice.getText().toString()));
        params.add(new BasicNameValuePair("sMinOrder", txtMinOrder.getText().toString()));
        params.add(new BasicNameValuePair("sAddress", txtAddress.getText().toString()));
        params.add(new BasicNameValuePair("sRid", RID));
        params.add(new BasicNameValuePair("sImage", encodeedImage));

        String resultServer  = getHttpPost(url,params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
        }
        else
        {
            Toast.makeText(EditResterrant.this, "Update Data Successfully", Toast.LENGTH_SHORT).show();
            txtNameStore.setText("");
            txtDetail.setText("");
            txtTime.setText("");
            txtPrice.setText("");
            txtMinOrder.setText("");
            txtAddress.setText("");
        }


        return true;
    }


    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/




    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
