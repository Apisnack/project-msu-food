package com.example.snackz.project;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Basket extends AppCompatActivity {
    String Uid;
    String Many = "";
    String Sumprice = "";
    String Rid;
    String MemberID;
    int sumTotal = 0;
    String Buy ="";
    SQLiteDatabase db;
    String strSumTotal="";
    ArrayList<HashMap<String, String>> MebmerList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent= getIntent();
        Rid = intent.getStringExtra("RID");
        Uid = intent.getStringExtra("UID");
        System.out.println("RIDDDDDDDDDDDDDDDD"+Rid);
        System.out.println("UIDDDDDDDDDDDDDDDD"+Uid);

       ShowOder();
       SumTotal();

        final Button ButClear =(Button) findViewById(R.id.EE);
        ButClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearBasket();
                ShowOder();
                final TextView strTotal = (TextView) findViewById(R.id.tSum);
                strTotal.setText("0");
                strSumTotal="";
                Buy ="";
            }
        });



    }


    public void ShowOder(){
        final myDBClass myDb = new myDBClass(this);
        MebmerList = myDb.SelectAllData();
        // listView1
        ListView lisView1 = (ListView)findViewById(R.id.listViewBasket);

        SimpleAdapter sAdap;
        sAdap = new SimpleAdapter(Basket.this, MebmerList, R.layout.column_basket,
                new String[] {"Name", "Many", "Price","OID"}, new int[] {R.id.NameMB, R.id.Many, R.id.PriceMB});
        lisView1.setAdapter(sAdap);
        registerForContextMenu(lisView1);
    }

    public void SumTotal(){

        final TextView strTotal = (TextView) findViewById(R.id.tSum);
        final myDBClass myDb = new myDBClass(this);

        // Perform action on click
        String arrData[][] = myDb.SelectAll();
        if(arrData == null)
        {
            //Toast.makeText(Basket.this,"Not found Data!",Toast.LENGTH_LONG).show();
        }
        else
        {
            int i ;
            int count [] = new int[30];
            String strTobuy[]= new String[30];
            for(i = 0;i<=arrData.length-1;i++) {
                count[i] = Integer.parseInt(arrData[i][4]);
                strTobuy[i]=arrData[i][1]+" X "+arrData[i][2]+" = "+arrData[i][4]+"\n";
                sumTotal = sumTotal + count[i];
                Buy = Buy + strTobuy[i];
               Toast.makeText(Basket.this,"Price :"+i+" = "+ arrData[i][1]+" X "+arrData[i][2]+" = "+arrData[i][4],Toast.LENGTH_LONG).show();
            }
        }

        strSumTotal = Integer.toString(sumTotal);
        //Toast.makeText(Basket.this,"SumTotal = "+strSumTotal ,Toast.LENGTH_LONG).show();
       // Toast.makeText(Basket.this,"Text = "+ Buy ,Toast.LENGTH_LONG).show();
        strTotal.setText(strSumTotal);
    }

   public void ClearBasket() {
       final myDBClass myDb = new myDBClass(this);
       myDb.DeleteDataAll();

   }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //if (v.getId()==R.id.list) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle(" Delete Menu !! "+ MebmerList.get(info.position).get("Name"));
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        for (int i = 0; i<menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }

    }

   public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        String CmdName = menuItems[menuItemIndex];
        String MemID = MebmerList.get(info.position).get("OID");
        //String MemName = MebmerList.get(info.position).get("Name").toString();

        // Check Event Command
        if ("Edit".equals(CmdName)) {

            // Show on new activity
            //Intent newActivity = new Intent(ListDeleteActivity.this,UpdateActivity.class);
            //newActivity.putExtra("MemID", MebmerList.get(info.position).get("ID").toString());
            //startActivity(newActivity);
        }
       else if ("Delete".equals(CmdName)) {

            myDBClass myDb = new myDBClass(this);

            long flg = myDb.DeleteData(MemID);
            if(flg > 0)
            {
                Toast.makeText(Basket.this,"Delete Data Successfully",
                        Toast.LENGTH_LONG).show();

                Buy ="";
                sumTotal=0;

                ShowOder();
                SumTotal();


            }
            else
            {
                Toast.makeText(Basket.this,"Delete Data Failed.",
                        Toast.LENGTH_LONG).show();


            }

            // Call Show Data again

        }

        return true;
    }
    public void Buy(View view) {
        Intent newActivity = new Intent(Basket.this, Buy.class);
        newActivity.putExtra("TestTotal", Buy);
        newActivity.putExtra("Sumprice", strSumTotal);
        newActivity.putExtra("RID", Rid);
        startActivity(newActivity);
    }
}
