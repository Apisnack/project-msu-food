package com.example.snackz.project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowOrder extends AppCompatActivity {
    String Server=Sever.sever;
    String strOID;
    String RID;
    String status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_order);


        Intent intent= getIntent();
        RID = intent.getStringExtra("RID");
        ShowOrderList();

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void ShowOrderList() {

        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listOrder);

        String url = Server+"ShowOrder.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid", RID));
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("oid", c.getString("oid"));
                map.put("orderlist", c.getString("orderlist"));
                map.put("price", c.getString("price"));
                map.put("phone", c.getString("phone"));
                map.put("address", c.getString("address"));
                map.put("latitude", c.getString("latitude"));
                map.put("longtitude", c.getString("longtitude"));
                map.put("rid", c.getString("rid"));
                map.put("uid", c.getString("uid"));
                map.put("status", c.getString("status"));
                map.put("datetime", c.getString("DateTime"));
                MyArrList.add(map);

            }

            lisView1.setAdapter(new ShowOrder.ImageAdapter(this,MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    strOID = MyArrList.get(position).get("oid").toString();
                    Intent newActivity = new Intent(ShowOrder.this, ShowInforOrder.class);
                    newActivity.putExtra("oid", strOID);
                    startActivity(newActivity);

                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public class ImageAdapter extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_order, null);
            }

                TextView txtPicName = (TextView) convertView.findViewById(R.id.OrderName);
                txtPicName.setPadding(5, 0, 0, 0);
                txtPicName.setText( MyArr.get(position).get("orderlist"));

                TextView txtPicName1 = (TextView) convertView.findViewById(R.id.OrderPrice);
                txtPicName1.setPadding(5, 0, 0, 0);
                txtPicName1.setText(MyArr.get(position).get("price")+" บาท");

                 status = MyArr.get(position).get("status").toString();

                if (status.equals("0")){
                    TextView txtPicName2 = (TextView) convertView.findViewById(R.id.Status);
                    txtPicName2.setPadding(5, 0, 0, 0);
                    txtPicName2.setText("รอดำเนินการ");
                    txtPicName2.setTextColor(Color.parseColor("#fce536"));
                }
                else if(status.equals("1")){
                    TextView txtPicName2 = (TextView) convertView.findViewById(R.id.Status);
                    txtPicName2.setPadding(5, 0, 0, 0);
                    txtPicName2.setText("ยืนยันการสั่งซื้อ");
                    txtPicName2.setTextColor(Color.parseColor("#77be1b"));

                }
                else if (status.equals("2")){
                    TextView txtPicName2 = (TextView) convertView.findViewById(R.id.Status);
                    txtPicName2.setPadding(5, 0, 0, 0);
                    txtPicName2.setText("ยกเลิกการสั่งซื้อ");
                    txtPicName2.setTextColor(Color.parseColor("#FFFA3E3E"));
                }

            TextView txtPicName2 = (TextView) convertView.findViewById(R.id.Datetime);
            txtPicName2.setPadding(5, 0, 0, 0);
            txtPicName2.setText(MyArr.get(position).get("datetime"));


            return convertView;

        }

    }

    public String getJSONUrl(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download file..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
