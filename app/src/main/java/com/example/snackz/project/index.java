package com.example.snackz.project;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class index extends AppCompatActivity {
    private Handler objHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);
        //*** Get Login Status
        if(!usrHelper.getLoginStatus())
        {
            objHandler = new Handler();
            objHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent objIntent = new Intent(index.this, MainActivity.class);
                    startActivity(objIntent);
                    finish();
                }
            }, 2500);
        }
        else{
            objHandler = new Handler();
            objHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent newActivity = new Intent(index.this, Home.class);
                    newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newActivity);
                    finish();
                }
            }, 2000);

        }
    }
}
