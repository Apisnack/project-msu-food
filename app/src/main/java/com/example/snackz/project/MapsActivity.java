package com.example.snackz.project;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap mMap;
    LocationManager locationManager;
    String mprovider;
    String UID;
    String RID;
    String Sumprice,Lat,Long,srtText1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            Location location = locationManager.getLastKnownLocation(mprovider);
            //locationManager.requestLocationUpdates(mprovider, 15000, 1,this);

            if (location != null)
                onLocationChanged(location);
            else
                Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
        }

        Intent intent= getIntent();
        UID = intent.getStringExtra("UID");
        RID = intent.getStringExtra("RID");

        Sumprice = intent.getStringExtra("Sumprice");
        srtText1 = intent.getStringExtra("TestTotal");

        System.out.println("RIDDDDDDDDDDDDDDDD"+RID);
        System.out.println("UIDDDDDDDDDDDDDDDD"+UID);




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //goToLocationZoom(15.716790, 102.574068, 15);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }
    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(this, "cant get location", Toast.LENGTH_LONG).show();
        } else {
            Lat = Double.toString(location.getLatitude());
            Long = Double.toString(location.getLongitude());
            System.out.println("LATTTTTTTTTTTTTTTTTTTTTT"+location.getLatitude());
            System.out.println("LONGGGGGGGGGGGGGGGGGGGGG"+location.getLongitude());
        }
    }
    public void getLocation(View view){
        Intent newActivity = new Intent(MapsActivity.this, Buy.class);
        newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        newActivity.putExtra("Lat", Lat);
        newActivity.putExtra("Long", Long);
        newActivity.putExtra("RID", RID);
        newActivity.putExtra("Sumprice", Sumprice);
        newActivity.putExtra("TestTotal",srtText1 );
        newActivity.putExtra("UID", UID);
        startActivity(newActivity);

    }
    public void UngetLocation(View view){
        Intent newActivity = new Intent(MapsActivity.this, Buy.class);
        newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        newActivity.putExtra("Sumprice", Sumprice);
        newActivity.putExtra("TestTotal",srtText1 );
        newActivity.putExtra("sRid", RID);
        newActivity.putExtra("MemberID", UID);
        startActivity(newActivity);

    }


}








