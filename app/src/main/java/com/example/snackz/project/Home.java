package com.example.snackz.project;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.R.attr.bitmap;
import static com.android.volley.VolleyLog.TAG;

public class Home extends AppCompatActivity  {
    TabHost mTabHost;
    String MemberID;
    String strRID,strMID,strSelect;
    String strUID;
    String Server=Sever.sever;
    String Many = "";
    private Bitmap bitmap;
    String strMemberID = "";
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        myDBClass myDb = new myDBClass(this);
        myDb.getWritableDatabase(); // First method


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Homedata();
                pullToRefresh.setRefreshing(false);
            }
        });

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        mTabHost.addTab(mTabHost.newTabSpec("tab_test1").setIndicator("หน้าหลัก").setContent(R.id.tab1));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2").setIndicator("ค้นหา").setContent(R.id.tab2));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2").setIndicator("โปรไฟล์").setContent(R.id.tab3));
        //mTabHost.addTab(mTabHost.newTabSpec("tab_test4").setIndicator("Profile").setContent(R.id.tab4));
        mTabHost.setCurrentTab(0);
        // Permission StrictMode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Login Status
        if(!usrHelper.getLoginStatus())
        {
            Intent newActivity = new Intent(Home.this, MainActivity.class);
            newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newActivity);
        }

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();

        Homedata();
        showInfo();
        UpdateToken();

        //*** Button Logout
        final Button btnLogout = (Button) findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Clear Session
                usrHelper.deleteSession();

                // Goto Acitity2

                LoginManager.getInstance().logOut();
                Intent newActivity = new Intent(Home.this,MainActivity.class);
                newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newActivity);
            }
        });

        final ImageView btn1 = (ImageView) findViewById(R.id.search);
        // Perform action on click
        btn1.setOnClickListener(new View.OnClickListener()

                                {
                                    public void onClick(View v) {
                                        //   System.out.println("22222222222222222222222222222222222222222222222");
                                        SearchData();
                                    }
                                }

        );


    }



    public void onRadioButtonClicked2(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioSelect1:
                if (checked)
                    strSelect = "1";
                //System.out.println(gender);
                Toast.makeText(Home.this, "ชื่อร้าน", Toast.LENGTH_SHORT).show();
                break;
            case R.id.radioSelect2:
                if (checked)
                    strSelect = "2";
                //System.out.println(gender);
                Toast.makeText(Home.this, "ชื่อเมนู", Toast.LENGTH_SHORT).show();
                break;

            case R.id.radioSelect3:
                if (checked)

                    strSelect = "3";
                //System.out.println(gender);
                Toast.makeText(Home.this, "ชื่อเมนูและชื่อร้าน", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void Homedata() {

        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listView5555);

        // editText1
        final EditText inputText = (EditText) findViewById(R.id.editText1);

        // Disbled Keyboard auto focus
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputText.getWindowToken(), 0);


        //System.out.println("XXXXXXXXXXXXXXXXXXXXXX555555555555555555555XXXXXXXXXXXXXXXXXXX");
        String url = Server+"getJSON.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONArray data = new JSONArray(getHttpPost(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("rid", c.getString("rid"));
                map.put("rname", c.getString("rname"));
                map.put("detail", c.getString("detail"));
                map.put("minorder", c.getString("minorder"));
                map.put("time", c.getString("time"));
                map.put("Cost", c.getString("Cost"));
                map.put("raddress", c.getString("raddress"));
                map.put("rpicture", c.getString("rpicture"));
                map.put("status", c.getString("status"));
                map.put("uid", c.getString("uid"));
                map.put("rating", c.getString("Rating"));
                MyArrList.add(map);

            }

            lisView1.setAdapter(new ImageAdapter(this,MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    strRID = MyArrList.get(position).get("rid").toString();
                    strUID = MyArrList.get(position).get("uid").toString();
                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(Home.this, ResterrantActivity.class);
                    newActivity.putExtra("rid", strRID);
                    newActivity.putExtra("uid", strUID);
                    newActivity.putExtra("MemberID",MemberID);
                    startActivity(newActivity);

                }
            });



        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void showInfo() {
        /* Profile */
        // txtMemberID,txtMemberID,txtUsername,txtPassword,txtName,txtEmail,txtTel
        final TextView tE = (TextView) findViewById(R.id.txtE);
        final TextView tN = (TextView) findViewById(R.id.txtN);
        final TextView tG = (TextView) findViewById(R.id.txtG);
        final TextView tT = (TextView) findViewById(R.id.txtT);
        final TextView tA = (TextView) findViewById(R.id.txtA);
        final ImageView iM = (ImageView) findViewById(R.id.PhotoProfile2) ;

        String url = Server+"getByMemberID.php";

        // Intent intent = getIntent();
        // MemberID = intent.getStringExtra("MemberID");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", strMemberID));

        String resultServer = getHttpPost(url, params);

        String strUID = "";
        String strEmail = "";
        String strName = "";
        String strGender = "";
        String strPhone = "";
        String strAddress = "";
        String strPhoto = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strUID = c.getString("UID");
            strEmail = c.getString("Email");
            strName = c.getString("Name");
            strGender = c.getString("Gender");
            strPhone = c.getString("Phone");
            strAddress = c.getString("Address");
            strPhoto = c.getString("Picture");
            //System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            if (!strUID.equals("")) {
                tE.setText(strEmail);
                tN.setText(strName);
                tG.setText(strGender);
                tT.setText(strPhone);
                tA.setText(strAddress);

                bitmap=loadBitmap(Server+strPhoto);
                iM.setImageBitmap(bitmap);

            } else {
                tE.setText("-");
                tN.setText("-");
                tG.setText("-");
                tT.setText("-");
                tA.setText("-");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public void SearchData() {
        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listViewSearch);

        // editText1
        final EditText inputText = (EditText) findViewById(R.id.editText1);
        final EditText inputText2 = (EditText) findViewById(R.id.editText2);
        // Disbled Keyboard auto focus
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputText.getWindowToken(), 0);

        String url = Server+"SearchData.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("txtKeyword", inputText.getText().toString()));
            params.add(new BasicNameValuePair("txtKeyword2", inputText2.getText().toString()));
            params.add(new BasicNameValuePair("sSelect", strSelect));

        ///////////////////////////////////////////////////////////Restaurant
        try {
            JSONArray data = new JSONArray(getHttpPost(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("rid", c.getString("rid"));
                map.put("rname", c.getString("rname"));
                map.put("detail", c.getString("detail"));
                map.put("status", c.getString("status"));
                map.put("minorder", c.getString("minorder"));
                map.put("time", c.getString("time"));
                map.put("Cost", c.getString("Cost"));
                map.put("raddress", c.getString("raddress"));
                map.put("rpicture", c.getString("rpicture"));
                map.put("uid", c.getString("uid"));
                MyArrList.add(map);

            }

            lisView1.setAdapter(new ImageAdapter(this,MyArrList));

            // OnClick Item
            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    strRID = MyArrList.get(position).get("rid").toString();
                    strUID = MyArrList.get(position).get("uid").toString();
                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(Home.this, ResterrantActivity.class);
                    newActivity.putExtra("rid", strRID);
                    newActivity.putExtra("uid", strUID);
                    startActivity(newActivity);

                }
            });
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //////////////////////////////////////////////////////////////////////////////Menu
        try {
            JSONArray data = new JSONArray(getHttpPost(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("mid", c.getString("mid"));
                map.put("mname", c.getString("mname"));
                map.put("detail", c.getString("detail"));
                map.put("price", c.getString("price"));
                map.put("picture", c.getString("picture"));
                map.put("type", c.getString("type"));
                map.put("rid", c.getString("rid"));
                MyArrList.add(map);

            }

            lisView1.setAdapter(new ImageAdapter2(this,MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    // strMID = MyArrList.get(position).get("mid").toString();
                    strRID = MyArrList.get(position).get("rid").toString();

                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(Home.this, ResterrantActivity.class);
                    newActivity.putExtra("rid", strRID);
                    startActivity(newActivity);

                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public boolean UpdateToken(){
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Token : " + refreshedToken);
            //Toast.makeText(Home.this, "Token : " + refreshedToken, Toast.LENGTH_SHORT).show();

        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"upDateToken.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID",strMemberID));
        params.add(new BasicNameValuePair("sToken", refreshedToken));


        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
           // Toast.makeText(Home.this, "Insert Token Success", Toast.LENGTH_SHORT).show();
        }


        return true;

    }



    public class ImageAdapter extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.column, null);
            }


            // ColImage
            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPath);
            imageView.getLayoutParams().height = 200;
            imageView.getLayoutParams().width = 200;
            String strPhoto = MyArr.get(position).get("rpicture").toString();


            bitmap=loadBitmap(Server+strPhoto);
            imageView.setImageBitmap(bitmap);
           /* try
            {
                String url = Server+"getByMemberID.php";
                UID = MyArr.get(position).get("uid");
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("sMemberID", UID));

                String resultServer = getHttpPost(url, params);
                String strPhoto = "";
                String strUID = "";
                JSONObject c;
                    c = new JSONObject(resultServer);
                    strUID = c.getString("UID");
                    strPhoto = c.getString("Picture");
                  //  System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                    if (!strUID.equals("")) {
                        bitmap=loadBitmap(Server+strPhoto);
                        imageView.setImageBitmap(bitmap);
                    }

            } catch (Exception e) {
                imageView.setImageResource(android.R.drawable.ic_menu_report_image);
            }*/

         /*   try {
                String url = Server+"getByMemberID.php";
                String User = MyArr.get(position).get("uid");
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("sMemberID", User));
                String resultServer = getHttpPost(url, params);
                String strUID = "";
                JSONObject c;
                c = new JSONObject(resultServer);
                strUID = c.getString("Name");

                TextView txtPosition = (TextView) convertView.findViewById(R.id.ColUser);
                txtPosition.setPadding(10, 0, 0, 0);
                txtPosition.setText(strUID);

                // ColPicname
                  TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameR);
                  txtPicName.setPadding(50, 0, 0, 0);
                  txtPicName.setText( MyArr.get(position).get("rname"));

            TextView txtPicName1 = (TextView) convertView.findViewById(R.id.ColDetail);
            txtPicName1.setPadding(50, 0, 0, 0);
            txtPicName1.setText(MyArr.get(position).get("detail"));

             }catch (Exception e) {

            }*/

            // ColPicname
            TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameR);
            txtPicName.setPadding(10, 0, 0, 0);
            txtPicName.setText( MyArr.get(position).get("rname"));

            TextView txtPicName1 = (TextView) convertView.findViewById(R.id.ColDetail);
            txtPicName1.setPadding(10, 0, 0, 0);
            txtPicName1.setText(MyArr.get(position).get("detail"));

            TextView txtPicName2 = (TextView) convertView.findViewById(R.id.ColCost);
            txtPicName2.setPadding(10, 0, 0, 0);
            txtPicName2.setText(MyArr.get(position).get("Cost"));

            TextView txtPicName3 = (TextView) convertView.findViewById(R.id.ColMinOder);
            txtPicName3.setPadding(10, 0, 0, 0);
            txtPicName3.setText(MyArr.get(position).get("minorder"));

            TextView txtPicName4 = (TextView) convertView.findViewById(R.id.ColTime);
            txtPicName4.setPadding(10, 0, 0, 0);
            txtPicName4.setText(MyArr.get(position).get("time"));

            TextView txtPicName5 = (TextView) convertView.findViewById(R.id.ColRating);
            txtPicName5.setPadding(10, 0, 0, 0);
            txtPicName5.setText(MyArr.get(position).get("rating"));

            return convertView;

        }

    }


    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/






    public class ImageAdapter2 extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter2(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.culumn_resterrant, null);
            }

            // ColImage
            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPathR);
            imageView.getLayoutParams().height = 150;
            imageView.getLayoutParams().width = 150;
            String strPhoto;
            try
            {

                strPhoto = MyArr.get(position).get("picture");
                //System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                bitmap=loadBitmap(Server+strPhoto);
                imageView.setImageBitmap(bitmap);

            } catch (Exception e) {
                // When Error
                imageView.setImageResource(android.R.drawable.ic_menu_report_image);
            }
            // ColPosition
            // String url = "https://foodmsu.000webhostapp.com/Foodmsu/getByMemberID.php";

            // ColPicname
            TextView txtPicNameM = (TextView) convertView.findViewById(R.id.ColNameR);
            txtPicNameM.setPadding(50, 0, 0, 0);
            txtPicNameM.setText( MyArr.get(position).get("mname"));

            TextView txtDetailM = (TextView) convertView.findViewById(R.id.ColDetailR);
            txtDetailM.setPadding(50, 0, 0, 0);
            txtDetailM.setText(MyArr.get(position).get("detail"));

            TextView txtPriceM = (TextView) convertView.findViewById(R.id.ColPriceR);
            txtPriceM.setPadding(50, 0, 0, 0);
            txtPriceM.setText( MyArr.get(position).get("price"));

            TextView txtTypeM = (TextView) convertView.findViewById(R.id.ColTypeR);
            txtTypeM.setPadding(50, 0, 0, 0);
            txtTypeM.setText(MyArr.get(position).get("type"));


            return convertView;

        }

    }



    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public void NextNewStore(View view) {

        Intent newActivity = new Intent(Home.this, New.class);
        newActivity.putExtra("MemberID", strMemberID);
        startActivity(newActivity);
    }
    public void manageResterrant(View view) {

        Intent newActivity = new Intent(Home.this, manageResterrant.class);
        newActivity.putExtra("MemberID", strMemberID);
        startActivity(newActivity);
    }
    public void EditProfile(View view){
        Intent newActivity = new Intent(Home.this, UpdateActivity.class);
        newActivity.putExtra("MemberID", strMemberID);
        startActivity(newActivity);

    }

    public void History(View view){
        Intent newActivity = new Intent(Home.this, OrderHistory.class);
        newActivity.putExtra("MemberID", strMemberID);
        startActivity(newActivity);

    }

    public void goTofaverite(View view){
        Intent newActivity = new Intent(Home.this, Favourite.class);
        newActivity.putExtra("MemberID", strMemberID);
        startActivity(newActivity);

    }

}

