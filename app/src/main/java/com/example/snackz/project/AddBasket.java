package com.example.snackz.project;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddBasket extends AppCompatActivity {
    ImageView txtImage;
    String MID,strMid,UID,RID;
    String Server=Sever.sever;
    Bitmap bitmap;
    String UserID;
    Button upButton;
    Button downButton;
    EditText editText;
    int uprange = 99;
    int downrange = 1;
    int values = 1;
    int NUM ;
    int Sumprice = 0;
    int intPrice ;
    String strNameSpe ;
    String strPrice ;
    String txtName ="";
    String NameMenu="";
    String strSID ;
    String Many;
    String strP;
    String strRid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_basket);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        ShowInforMenu();
        //ShowSpeMenu();

        final Button AddB =(Button) findViewById(R.id.AddToBasket);
        AddB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BAddBasket())
                {
                    // Open Form Main
                    Intent intent = getIntent();
                    UserID = intent.getStringExtra("User");

                }
            }
        });





        upButton = (Button) findViewById(R.id.Pl);
        downButton = (Button) findViewById(R.id.DE);
        editText = (EditText) findViewById(R.id.display);

        upButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (values >= downrange && values <= uprange)
                    values++;
                if (values > uprange)
                    values = downrange;
                editText.setText("" + values);
                //

            }
        });

        downButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (values >= downrange && values <= uprange)
                    values--;

                if (values < downrange)
                    values = uprange;

                editText.setText(values + "");
            }
        });

    }

   public boolean BAddBasket(){
        // Intent newActivity = new Intent(AddBasket.this, Home.class);

        Many ="";
        int Sumprice;

        Sumprice = intPrice*values;
        strP = Integer.toString(Sumprice);
        Many = Integer.toString(values);
        // Dialog
       final AlertDialog.Builder adb = new AlertDialog.Builder(this);
       AlertDialog ad = adb.create();
        final myDBClass myDb = new myDBClass(this);
       // Check Data (MemberID exists)
       String arrData[] = myDb.SelectData(strRid);
       if(arrData != null)
       {
           ad.setMessage(" ไม่สามารถสั่งอาหารหลายร้านได้ !! ");
           ad.show();
           //strRid.requestFocus();
           return false;
       }


        // Save Data
       long saveStatus = myDb.InsertData(NameMenu,Many,strP,strRid);
       if(saveStatus <=  0)
       {
           ad.setMessage("Error!! ");
           ad.show();
           return false;
       }

       Toast.makeText(AddBasket.this,"Add Data Successfully. ",
               Toast.LENGTH_SHORT).show();
       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
    public void ShowInforMenu(){

        txtImage = (ImageView)findViewById(R.id.BImage);
        final TextView txtName = (TextView)findViewById(R.id.BName);
        final TextView txtDetail = (TextView)findViewById(R.id.BDetail);
        final TextView txtType = (TextView)findViewById(R.id.BType);
        final TextView txtPrice = (TextView)findViewById(R.id.BPrice);
       // final TextView txtPrice2 = (TextView)findViewById(R.id.BPrice2);



        Intent intent = getIntent();
        MID = intent.getStringExtra("mid");

        String url = Server+"ShowInforMenu.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMid", MID));


        String resultServer = getHttpPost(url, params);
        strMid = "";
        String strDetail = "";
        String strType = "";
        String strPrice = "";
        String strPhoto = "";


        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strMid = c.getString("mid");
            NameMenu = c.getString("mname");
            strDetail = c.getString("detail");
            strType = c.getString("type");
            strPrice = c.getString("price");
            strPhoto = c.getString("picture");
            strRid = c.getString("rid");
            intPrice = Integer.parseInt(strPrice);

            //System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            if (!strMid.equals("")) {
                txtName.setText(NameMenu);
                txtDetail.setText(strDetail);
                txtType.setText(strType);
                txtPrice.setText(strPrice);
                bitmap=loadBitmap(Server+strPhoto);
                txtImage.setImageBitmap(bitmap);

            } else {
                txtName.setText("-");
                txtDetail.setText("-");
                txtType.setText("-");
                txtPrice.setText("-");
               // txtPrice2.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
