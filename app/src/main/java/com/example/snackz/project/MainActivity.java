package com.example.snackz.project;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kosalgeek.android.photoutil.ImageBase64;


public class MainActivity extends AppCompatActivity {
    String Server=Sever.sever;
    private LoginButton btnFaceBookLogin;
    private CallbackManager callbackManager;
    String str_id ,str_name;
    String encodeedImage;
    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // btnLogin
        final Button btnLogin = (Button) findViewById(R.id.Login);
        // Perform action on click
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckLogin();
            }
        });
     // printKeyHash();
        LoginFacebook();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }
    public static Bitmap getFacebookProfilePicture(String userID){
        Bitmap bitmap = null;
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?width=250&height=250");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void LoginFacebook(){

        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        btnFaceBookLogin = (LoginButton) findViewById(R.id.login_button);
        btnFaceBookLogin.setReadPermissions(Arrays.asList("user_photos","email","public_profile"));
        final UserHelper usrHelper = new UserHelper(this);
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        btnFaceBookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(MainActivity.this,"Success "+ loginResult.getAccessToken().getUserId(),Toast.LENGTH_SHORT).show();

                Bundle parameter = new Bundle();
                parameter.putString("fields", "id,name,picture");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                             str_id = object.getString("id");
                           // Toast.makeText(MainActivity.this,str_id,Toast.LENGTH_LONG).show();
                            Log.e("id",str_id);

                           // String str_email = object.getString("email");
                           // Toast.makeText(MainActivity.this,str_email,Toast.LENGTH_LONG).show();
                           // Log.e("email",str_email);

                             str_name = object.getString("name");
                           // Toast.makeText(MainActivity.this,str_name,Toast.LENGTH_LONG).show();
                            Log.e("name",str_name);

                           // String str_picture = object.getString("picture");
                           // Toast.makeText(MainActivity.this,str_picture,Toast.LENGTH_LONG).show();
                           //  Log.e("picture",str_picture);

                            Bitmap bitmap = getFacebookProfilePicture(str_id);
                            encodeedImage = ImageBase64.encode(bitmap);
                            Log.e("encodeedImage",encodeedImage);



                            List<NameValuePair> params = new ArrayList<NameValuePair>();
                            params.add(new BasicNameValuePair("strUser",str_id));
                            params.add(new BasicNameValuePair("strPass",str_id));
                            params.add(new BasicNameValuePair("sName", str_name));
                            params.add(new BasicNameValuePair("sImage", encodeedImage));

                            String url = Server+"checkLoginFacebook.php";
                            String resultServer  = getHttpPost(url,params);

                            /*** Default Value ***/
                            String strStatusID = "0";
                            String strMemberID = "0";
                            String strError = "Unknow Status!";

                            JSONObject c;
                            try {
                                c = new JSONObject(resultServer);
                                strStatusID = c.getString("StatusID");
                                strMemberID = c.getString("MemberID");
                                strError = c.getString("Error");

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }



                            // Prepare Login
                            if(strStatusID.equals("0"))
                            {
                                // Dialog
                                ad.setTitle("Error! ");
                                ad.setIcon(android.R.drawable.btn_star_big_on);
                                ad.setPositiveButton("Close", null);
                                ad.setMessage(strError);
                                ad.show();

                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, "Login OK", Toast.LENGTH_SHORT).show();
                                usrHelper.createSession(strMemberID);
                                Intent newActivity = new Intent(MainActivity.this,Home.class);
                                newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(newActivity);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                request.setParameters(parameter);
                request.executeAsync();


               // goHomeScreen();
              //  CheckLoginFacebook();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, "Error "+error,Toast.LENGTH_SHORT).show();
            }
        });

    }






    private void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.snackz.project", PackageManager.GET_SIGNATURES);
            for (Signature signature:info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(),Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void CheckLogin(){

        final UserHelper usrHelper = new UserHelper(this);
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        // txtUsername & txtPassword
        final EditText txtUser = (EditText)findViewById(R.id.user);
        final EditText txtPass = (EditText)findViewById(R.id.pass);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("strUser", txtUser.getText().toString()));
        params.add(new BasicNameValuePair("strPass", txtPass.getText().toString()));

        String url = Server+"checkLogin.php";
        String resultServer  = getHttpPost(url,params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strMemberID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strMemberID = c.getString("MemberID");
            strError = c.getString("Error");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        // Prepare Login
        if(strStatusID.equals("0"))
        {
            // Dialog
            ad.setTitle("Error! ");
            ad.setIcon(android.R.drawable.btn_star_big_on);
            ad.setPositiveButton("Close", null);
            ad.setMessage(strError);
            ad.show();
            txtUser.setText("");
            txtPass.setText("");
        }
        else
        {
            Toast.makeText(MainActivity.this, "Login OK", Toast.LENGTH_SHORT).show();
            usrHelper.createSession(strMemberID);
            Intent newActivity = new Intent(MainActivity.this,Home.class);
            newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newActivity);
        }
    }


    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }



    public void NextRegister (View view){
        Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
        startActivity(intent);
    }
}

