package com.example.snackz.project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Buy extends AppCompatActivity {

    String Server=Sever.sever;
    String Sumprice,Lat,Long,Rid,Uid,Name,Many,strText;
    String strUID = "";
    String strMemberID = "";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent= getIntent();
        Rid = intent.getStringExtra("RID");
        Uid = intent.getStringExtra("UID");
        System.out.println("RIDDDDDDDDDDDDDDDD"+Rid);
        System.out.println("UIDDDDDDDDDDDDDDDD"+Uid);

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();

        strText = intent.getStringExtra("TestTotal");
        Sumprice = intent.getStringExtra("Sumprice");


        Lat = intent.getStringExtra("Lat");
        Long = intent.getStringExtra("Long");

        ShowCost();

        final TextView setT1 =(TextView)findViewById(R.id.tName);
        final TextView latti =(TextView)findViewById(R.id.tLat);
        final TextView longti =(TextView)findViewById(R.id.tLong);

        final EditText tPhone = (EditText) findViewById(R.id.tPhone);
        final EditText tAddress = (EditText) findViewById(R.id.tAddress);

        // Disbled Keyboard auto focus
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tPhone.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(tAddress.getWindowToken(), 0);

        latti.setText(Lat);
        longti.setText(Long);

        setT1.setText(strText);

        Button buy = (Button)findViewById(R.id.buy);
        Button canbuy = (Button)findViewById(R.id.canbuy);

        getUIDfromRID();

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Savedata();


            }
        });

        canbuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void ShowCost(){

        final TextView tCost = (TextView)findViewById(R.id.tCost);
        final TextView setPrice =(TextView)findViewById(R.id.tPrice);

        String url = Server+"ShowCost.php";

        // Intent intent = getIntent();
        // MemberID = intent.getStringExtra("MemberID");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid", Rid));

        String resultServer = getHttpPost(url, params);

        String strCost = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strCost = c.getString("Cost");

            int cost = Integer.parseInt(strCost);
            int SumPrice = Integer.parseInt(Sumprice);
            int SumPriceAll ;

            SumPriceAll = SumPrice+cost;

            String strSumPrice = Integer.toString(SumPriceAll);

            //Sumprice = Sumprice+cost;

            if (!strCost.equals("")) {
                tCost.setText(strCost);
                setPrice.setText(strSumPrice);
            } else {

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean Savedata() {
        final TextView tName = (TextView) findViewById(R.id.tName);
        final TextView tPrice = (TextView) findViewById(R.id.tPrice);
        final TextView tLat = (TextView) findViewById(R.id.tLat);
        final TextView tLong = (TextView) findViewById(R.id.tLong);
        final EditText tPhone = (EditText) findViewById(R.id.tPhone);
        final EditText tRemark = (EditText) findViewById(R.id.tRemark);
        final EditText tAddress = (EditText) findViewById(R.id.tAddress);
        //String ImageData = imageToString(bitmap);

        // Current Date
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String formattedDate = df.format(calendar.getTime());

        Toast.makeText(Buy.this, "Date Time" + formattedDate , Toast.LENGTH_SHORT).show();



        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Username
        if (tPhone.getText().length() == 0) {
            ad.setMessage("Please input [Phone Number] ");
            ad.show();
            tPhone.requestFocus();
            return false;
        }
        // Check Username
        if (tLat.getText().length() == 0) {
            ad.setMessage("Please input [Location] ");
            ad.show();
            tPhone.requestFocus();
            return false;
        }
        // Check Password
        if (tAddress.getText().length() == 0 ) {
            ad.setMessage("Please input [Address] ");
            ad.show();
            tAddress.requestFocus();
            return false;
        }

        String url = Server+"saveBuyData.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sN", tName.getText().toString()));
        params.add(new BasicNameValuePair("sP", tPrice.getText().toString()));
        params.add(new BasicNameValuePair("sLat", tLat.getText().toString()));
        params.add(new BasicNameValuePair("sLong", tLong.getText().toString()));
        params.add(new BasicNameValuePair("sPhone", tPhone.getText().toString()));
        params.add(new BasicNameValuePair("sAddress", tAddress.getText().toString()));
        params.add(new BasicNameValuePair("sRemark", tRemark.getText().toString()));
        params.add(new BasicNameValuePair("sRid", Rid));
        params.add(new BasicNameValuePair("sUid", strMemberID));
        params.add(new BasicNameValuePair("sDateTime", formattedDate));

        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(Buy.this, "Buy Successfully", Toast.LENGTH_SHORT).show();
            tPhone.setText("");
            tAddress.setText("");

            Intent intent = new Intent(Buy.this, Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("MemberID", Uid);
            ClearDataBasket();
            Send_Notification();
            startActivity(intent);

        }
        return true;
    }


   public void ClearDataBasket(){
        final myDBClass myDb = new myDBClass(this);
        myDb.DeleteDataAll();
    }

    public void getUIDfromRID(){
        String url = Server + "getUidFromRid.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", Rid));

        String resultServer = getHttpPost(url, params);

        JSONObject c;

        try {
            c = new JSONObject(resultServer);
            strUID = c.getString("UID");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void Send_Notification(){
        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"send_noti_restterant.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sUID",strUID));
        getHttpPost(url,params);
        Toast.makeText(this, "Send Notification", Toast.LENGTH_SHORT).show();



    }

    public void ShowLocation(View view){
        Intent newActivity = new Intent(Buy.this, MapsActivity.class);
        newActivity.putExtra("UID", Uid);
        newActivity.putExtra("RID", Rid);
        newActivity.putExtra("Name", Name);
        newActivity.putExtra("Many", Many);
        newActivity.putExtra("Sumprice", Sumprice);
        newActivity.putExtra("TestTotal", strText);
        startActivity(newActivity);

    }

    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}