package com.example.snackz.project;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResterrantActivity extends AppCompatActivity {

    String UID;
    String RID;
    String Server =Sever.sever;
    String User;
    String MID;
    public TextView txtView;
    private Bitmap bitmap;
    float rating;
    float RatingComment;
    String strRating = "";
    TextView item1;
    RatingBar Voterating;
    Switch simpleSwitch;
    String strMemberID = "";
    String strRid = "";
    String strUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resterrant);



        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


       /* final Button ShowOrder = (Button) findViewById(R.id.fab);
        ShowOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newActivity = new Intent(ResterrantActivity.this,Basket.class);
                startActivity(newActivity);
            }
        });*/

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();


        showRestterrant();
        showUser();
        ShowMenu();
        showRating();
        showReview();
        ShowFavourite();


        // initiate a Switch
        simpleSwitch = (Switch) findViewById(R.id.SFavourite);
        // simpleSwitch.setChecked(true);
        simpleSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check current state of a Switch (true or false).
                Boolean switchState = simpleSwitch.isChecked();

                if (switchState == true){
                    Toast.makeText( ResterrantActivity.this, "Add To Favourite", Toast.LENGTH_SHORT).show();
                    AddFavourite();
                }
                else if (switchState == false){
                    Toast.makeText( ResterrantActivity.this, "Delete From Favourite", Toast.LENGTH_SHORT).show();
                    UnFavourite();
                }
            }
        });


    }



    public void saveAVGrating() {
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"AVGrating.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", RID));
        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(ResterrantActivity.this, "Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void showReview(){
        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listComment);


        String url = Server + "ShowReview.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", RID));
        //System.out.println(RID+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("rv_id", c.getString("rv_id"));
                map.put("comment", c.getString("comment"));
                map.put("rating", c.getString("rating"));
                map.put("rid", c.getString("rid"));
                map.put("uid", c.getString("uid"));
                MyArrList.add(map);

            }


            lisView1.setAdapter(new ImageAdapter2(this, MyArrList));


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showRating() {
        // textView1
        final TextView txtView1 = (TextView) findViewById(R.id.textView1);
        // seekBar1
        final RatingBar rating_Bar = (RatingBar) findViewById(R.id.ratingBar1);

        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        //System.out.println(RID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server + "getRestterrant.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", RID));

        String resultServer = getHttpPost(url, params);

        // String strUID = "";
        String strRating = "";




        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            //  strUID = c.getString("Name");
            strRating = c.getString("Rating");
            if (!strRating.equals("")) {
                rating_Bar.setEnabled(false);
                rating_Bar.setMax(5);
                rating_Bar.setRating(Float.valueOf(strRating));
                txtView1.setText(strRating);
            } else {


            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        // Button1
       /* final Button btn1 = (Button) findViewById(R.id.vote);
        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowDialog();
            }
        });*/
    }


    public boolean ShowDialog(View view){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        final TextView txtComment = (TextView) findViewById(R.id.txtComment);
        final View Viewlayout = inflater.inflate(R.layout.popup_rating,
                (ViewGroup) findViewById(R.id.layout_dialog));
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        UID = intent.getStringExtra("uid");

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Email
        if (txtComment.getText().length() == 0) {
            ad.setMessage("Please input [Comment] ");
            ad.show();
            txtComment.requestFocus();
            return false;
        }

        popDialog.setTitle("Vote");
        Voterating = (RatingBar)Viewlayout.findViewById(R.id.ratingBar);

        popDialog.setView(Viewlayout);
        // Button OK
        popDialog.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        float rating = Voterating.getRating();
                        String strRating = Float.toString(rating);
                        String url = Server+"saveReviewData.php";

                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("sComment", txtComment.getText().toString()));
                        params.add(new BasicNameValuePair("sRating", strRating));
                        params.add(new BasicNameValuePair("sRID",RID ));
                        params.add(new BasicNameValuePair("sUID", strMemberID));

                        String resultServer = getHttpPost(url, params);

                        /*** Default Value ***/
                        String strStatusID = "0";
                        String strError = "Unknow Status!";

                        JSONObject c;
                        try {
                            c = new JSONObject(resultServer);
                            strStatusID = c.getString("StatusID");
                            strError = c.getString("Error");
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        // Prepare Save Data
                        if (strStatusID.equals("0")) {
                            ad.setMessage(strError);
                            ad.show();
                        } else {
                            Toast.makeText(ResterrantActivity.this, "Successfully", Toast.LENGTH_SHORT).show();
                            txtComment.setText("");
                            saveAVGrating();
                            Intent intent = new Intent(ResterrantActivity.this, ResterrantActivity.class);
                            intent.putExtra("rid", RID);
                            intent.putExtra("uid", UID);
                            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }


                        dialog.dismiss();
                    }

                });

        // Button Cancel
        popDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        popDialog.create();
        popDialog.show();


        return true;
    }




    public void showRestterrant() {

        final TextView tRN = (TextView) findViewById(R.id.trname);
        final TextView tDe = (TextView) findViewById(R.id.tde);
        final TextView tMin = (TextView) findViewById(R.id.tmin);
        final TextView tTime = (TextView) findViewById(R.id.tT);
        final TextView tAdd = (TextView) findViewById(R.id.tadd);
        final TextView tSts = (TextView) findViewById(R.id.StatusText);

        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        //System.out.println(RID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server + "getRestterrant.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRID", RID));

        String resultServer = getHttpPost(url, params);

        String strRname = "";
        String strDetail = "";
        String strMin = "";
        String strTime = "";
        String strRAddress = "";
        String strCost = "";
        String strStatus = "";


        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strRname = c.getString("RName");
            strDetail = c.getString("Detail");
            strMin = c.getString("MinOrder");
            strTime = c.getString("Time");
            strRAddress = c.getString("RAddress");
            strCost = c.getString("Cost");
            strUid = c.getString("uid");
            strStatus = c.getString("Status");

            if (!strRname.equals("")) {
                tRN.setText(strRname);
                tDe.setText(strDetail);
                tMin.setText(strMin);
                tTime.setText(strTime);
                tAdd.setText(strRAddress);

                if (strStatus.equals("1")){
                    tSts.setText("Online");
                    tSts.setTextColor(Color.parseColor("#77be1b"));
                }else if (strStatus.equals("0")){
                    tSts.setText("Offline");
                }
                // tRN.setText(strRname);

            } else {
                tRN.setText("-");
                tDe.setText("-");
                tMin.setText("-");
                tTime.setText("-");
                tAdd.setText("-");
                // tRN.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void showUser() {

        // final TextView tUname = (TextView) findViewById(R.id.tUname);
        final ImageView iM = (ImageView) findViewById(R.id.imageResterrant);

        // Intent intent = getIntent();
        // UID = intent.getStringExtra("uid");
        // System.out.println(UID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String url = Server + "getByMemberID.php";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sMemberID", strUid));


        String resultServer = getHttpPost(url, params);

        // String strUID = "";
        String strPhoto = "";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            //  strUID = c.getString("Name");
            strPhoto = c.getString("Picture");
            if (!strPhoto.equals("")) {
                //  tUname.setText(strUID);
                bitmap = loadBitmap(Server + strPhoto);
                iM.setImageBitmap(bitmap);
            } else {
                //   tUname.setText("-");

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean AddFavourite() {


        // Dialog
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"AddFavourite.php";
        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid",RID));
        params.add(new BasicNameValuePair("sUid",strMemberID));



        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
            Toast.makeText(ResterrantActivity.this, "Update Success", Toast.LENGTH_SHORT).show();
        }


        return true;
    }

    public boolean UnFavourite() {
        final android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        String url = Server+"UnFavourite.php";

        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sUid",strMemberID));
        params.add(new BasicNameValuePair("sRid",RID));



        String resultServer  = getHttpPost(url,params);


        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
            ad.setMessage(strError);
            ad.show();
            return false;
        }
        else
        {
            Toast.makeText(ResterrantActivity.this, "Update Success", Toast.LENGTH_SHORT).show();
        }


        return true;

    }

    public void ShowFavourite() {

        final Switch aSwitch = (Switch)findViewById(R.id.SFavourite);

        String url = Server + "ShowFavourite.php";

        Intent intent = getIntent();
        RID = intent.getStringExtra("rid");
        UID = intent.getStringExtra("uid");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid", RID));

        String resultServer = getHttpPost(url, params);


        String strUid = "";
        String strFid= "";
        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strFid = c.getString("sFid");
            strRid = c.getString("sRid");
            strUid = c.getString("sUid");

            if (!strFid.equals("")) {

                if (strUid.equals(strMemberID)) {
                    aSwitch.setChecked(true);
                }
                else {
                    aSwitch.setChecked(false);
                }
                // tRN.setText(strRname);

            } else {
                Toast.makeText(ResterrantActivity.this, "No data", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block

        }
    }

    public void ShowMenu() {
        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.listViewMenuR);

        Intent intent = getIntent();
        User = intent.getStringExtra("MemberID");

        String url = Server + "ShowMenu.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sRid", RID));
        //System.out.println(RID+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("mid", c.getString("mid"));
                map.put("mname", c.getString("mname"));
                map.put("detail", c.getString("detail"));
                map.put("price", c.getString("price"));
                map.put("picture", c.getString("picture"));
                map.put("type", c.getString("type"));
                map.put("rid", c.getString("rid"));
                MyArrList.add(map);

            }


            lisView1.setAdapter(new ImageAdapter(this, MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    MID = MyArrList.get(position).get("mid").toString();

                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(ResterrantActivity.this, AddBasket.class);
                    newActivity.putExtra("mid", MID);
                    newActivity.putExtra("User", User);
                    newActivity.putExtra("RID", RID);
                    newActivity.putExtra("UID", UID);
                    //  System.out.println(MID+"ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");

                    startActivity(newActivity);

                }
            });


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ImageAdapter2 extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter2(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_review, null);
            }

            // ColImage

            String UID = MyArr.get(position).get("uid");

            String url = Server+"getByMemberID.php";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sMemberID", UID));


            String resultServer = getHttpPost(url, params);

            String strUID = "";

            TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameReview);
            JSONObject c;
            try {
                c = new JSONObject(resultServer);
                strUID = c.getString("Name");

                if (!strUID.equals("")) {
                    txtPicName.setPadding(5, 0, 0, 0);
                    txtPicName.setText(strUID+" :");

                } else {
                    txtPicName.setText("-");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // ColPicname
            TextView txtPicNameM = (TextView) convertView.findViewById(R.id.ColComment);
            txtPicNameM.setPadding(5, 0, 0, 0);
            txtPicNameM.setText(MyArr.get(position).get("comment"));

            // ColratingBar
            RatingBar Rating = (RatingBar) convertView.findViewById(R.id.ratingReview);
            Rating.setPadding(10, 0, 0, 0);
            Rating.setEnabled(false);
            Rating.setMax(5);
            Rating.setRating(Float.valueOf(MyArr.get(position).get("rating").toString()));


            return convertView;

        }

    }


    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_menu, null);
            }

            // ColImage
            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPathM);
            imageView.getLayoutParams().height = 200;
            imageView.getLayoutParams().width = 200;
            String strPhoto;
            try {

                strPhoto = MyArr.get(position).get("picture");
                //System.out.println(strPhoto+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                bitmap = loadBitmap(Server + strPhoto);
                imageView.setImageBitmap(bitmap);

            } catch (Exception e) {
                // When Error
                imageView.setImageResource(android.R.drawable.ic_menu_report_image);
            }
            // ColPosition
            //String url = "https://foodmsu.000webhostapp.com/Foodmsu/getByMemberID.php";


            // ColPicname
            TextView txtPicNameM = (TextView) convertView.findViewById(R.id.ColNameM);
            txtPicNameM.setPadding(50, 0, 0, 0);
            txtPicNameM.setText(MyArr.get(position).get("mname"));

            TextView txtDetailM = (TextView) convertView.findViewById(R.id.ColDetailM);
            txtDetailM.setPadding(50, 0, 0, 0);
            txtDetailM.setText(MyArr.get(position).get("detail"));

            TextView txtPriceM = (TextView) convertView.findViewById(R.id.ColPriceM);
            txtPriceM.setPadding(50, 0, 0, 0);
            txtPriceM.setText(MyArr.get(position).get("price"));

            TextView txtTypeM = (TextView) convertView.findViewById(R.id.ColTypeM);
            txtTypeM.setPadding(50, 0, 0, 0);
            txtTypeM.setText(MyArr.get(position).get("type"));

            return convertView;

        }

    }

    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;

    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }

    public String getJSONUrl(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download file..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public void ShowOder(View view){
        Intent newActivity = new Intent(ResterrantActivity.this,Basket.class);
        newActivity.putExtra("RID", RID);
        newActivity.putExtra("uidUser", strMemberID);
        startActivity(newActivity);
    }

}
