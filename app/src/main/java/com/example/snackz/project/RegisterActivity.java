package com.example.snackz.project;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RegisterActivity extends AppCompatActivity {
    String gender = "1";
    private final String TAG = this.getClass().getName();
    ImageView selectphoto;
   // GalleryPhoto galleryPhoto;
    //String strPhoto;
    private static final int GALLERY_REQUEST = 22131;
    String Server=Sever.sever;
    Uri imgUri;
    Bitmap bitmap;
    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        // Permission StrictMode
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // btnSave
        final Button saveButton = (Button) findViewById(R.id.Bregister);
        // Perform action on click
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SaveData();
            }
        });
       // galleryPhoto = new GalleryPhoto(getApplicationContext());
        selectphoto = (ImageView) findViewById(R.id.ProfileImage);
        selectphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
              // startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
               openGallery();
            }

        });
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                selectphoto.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio1:
                if (checked)
                    gender = "1";
                //System.out.println(gender);
                break;
            case R.id.radio2:
                if (checked)
                    gender = "2";
                //System.out.println(gender);
                break;
        }
    }

    public boolean SaveData() {
        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);
        final EditText txtConPassword = (EditText) findViewById(R.id.txtConPassword);
        final EditText txtName = (EditText) findViewById(R.id.txtName);
        final EditText txtAddress = (EditText) findViewById(R.id.txtAddress);
        final EditText txtTel = (EditText) findViewById(R.id.txtTel);
        //String ImageData = imageToString(bitmap);

        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Email
        if (txtEmail.getText().length() == 0) {
            ad.setMessage("Please input [Email] ");
            ad.show();
            txtEmail.requestFocus();
            return false;
        }
        // Check Password
        if (txtPassword.getText().length() == 0 || txtConPassword.getText().length() == 0) {
            ad.setMessage("Please input [Password/Confirm Password] ");
            ad.show();
            txtPassword.requestFocus();
            return false;
        }
        // Check Password and Confirm Password (Match)
        if (!txtPassword.getText().toString().equals(txtConPassword.getText().toString())) {
            ad.setMessage("Password and Confirm Password Not Match! ");
            ad.show();
            txtConPassword.requestFocus();
            return false;
        }
        // Check Name
        if (txtName.getText().length() == 0) {
            ad.setMessage("Please input [Name] ");
            ad.show();
            txtName.requestFocus();
            return false;
        }
        // Check Address
        if (txtAddress.getText().length() == 0) {
            ad.setMessage("Please input [Address] ");
            ad.show();
            txtName.requestFocus();
            return false;
        }
        // Check Tel
        if (txtTel.getText().length() == 0) {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            txtTel.requestFocus();
            return false;
        }

        if (txtTel.getText().length() > 10) {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            txtTel.requestFocus();
            return false;
        }

        String url = Server+"saveADDData.php";

        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);
       /* try {
            Bitmap bitmap1 = ImageLoader.init().from(strPhoto).requestSize(1024, 1024).getBitmap();
            encodeedImage = ImageBase64.encode(bitmap1);
            Log.d(TAG, encodeedImage);


        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(),
                    "Something Wrong while encoding photos", Toast.LENGTH_SHORT).show();
        }*/

        //HashMap<String, String> postdata = new HashMap<String, String>();
        //postdata.put("sImage", encodeedImage);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sEmail", txtEmail.getText().toString()));
        params.add(new BasicNameValuePair("sPassword", txtPassword.getText().toString()));
        params.add(new BasicNameValuePair("sName", txtName.getText().toString()));
        params.add(new BasicNameValuePair("sAddress", txtAddress.getText().toString()));
        params.add(new BasicNameValuePair("sTel", txtTel.getText().toString()));
        params.add(new BasicNameValuePair("sGender", gender));
        params.add(new BasicNameValuePair("sImage", encodeedImage));

        //System.out.print(encodeedImage+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        // System.out.println(ImageData+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(RegisterActivity.this, "Register Successfully", Toast.LENGTH_SHORT).show();
            txtEmail.setText("");
            txtPassword.setText("");
            txtConPassword.setText("");
            txtName.setText("");
            txtAddress.setText("");
            txtTel.setText("");
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return true;
    }


    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
