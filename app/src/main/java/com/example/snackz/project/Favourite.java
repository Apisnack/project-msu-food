package com.example.snackz.project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Favourite extends AppCompatActivity {
    String MemberID;
    String strRID;
    String strUID;
    String Server=Sever.sever;
    String Many = "";
    private Bitmap bitmap;
    String strMemberID = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);

        //*** Get Session Login
        final UserHelper usrHelper = new UserHelper(this);

        //*** Get Member ID from Session
        strMemberID = usrHelper.getMemberID();


        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Homedata();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void Homedata() {

        // listView1
        final ListView lisView1 = (ListView) findViewById(R.id.Faverite_list);

        //System.out.println("XXXXXXXXXXXXXXXXXXXXXX555555555555555555555XXXXXXXXXXXXXXXXXXX");
        String url = Server+"getFavourite.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sUid",strMemberID));
        try {
            JSONArray data = new JSONArray(getJSONUrl(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("fid", c.getString("fid"));
                map.put("rid", c.getString("rid"));
                map.put("uid", c.getString("uid"));

                MyArrList.add(map);

            }

            lisView1.setAdapter(new ImageAdapter(this,MyArrList));

            lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                    strRID = MyArrList.get(position).get("rid").toString();
                    strUID = MyArrList.get(position).get("uid").toString();
                    //System.out.println(strRID+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    // System.out.println(strUID+"zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                    Intent newActivity = new Intent(Favourite.this, ResterrantActivity.class);
                    newActivity.putExtra("rid", strRID);
                    newActivity.putExtra("uid", strUID);
                    newActivity.putExtra("MemberID",MemberID);
                    startActivity(newActivity);

                }
            });



        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



    public class ImageAdapter extends BaseAdapter
    {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list)
        {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.colum_favourite, null);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPathFavourite);
            imageView.getLayoutParams().height = 200;
            imageView.getLayoutParams().width = 200;

            String strRID = MyArr.get(position).get("rid").toString();

            try {
                String url = Server+"getRestterrantFavourite.php";
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("sRID", strRID));
                String resultServer = getHttpPost(url, params);
                String strPicture = "";
                String strRn = "";
                String strDtail = "";
                String strMin = "";
                String strCost = "";
                JSONObject c;
                c = new JSONObject(resultServer);
                strRn = c.getString("RName");
                strDtail = c.getString("Detail");
                strMin = c.getString("MinOrder");
                strCost = c.getString("Cost");
                strPicture = c.getString("Picture");

                bitmap=loadBitmap(Server+strPicture);
                imageView.setImageBitmap(bitmap);

                // ColPicname
                TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameRFavourite);
                txtPicName.setPadding(50, 0, 0, 0);
                txtPicName.setText(strRn);

                TextView txtPicName1 = (TextView) convertView.findViewById(R.id.ColDetailFavourite);
                txtPicName1.setPadding(50, 0, 0, 0);
                txtPicName1.setText(strDtail);

                TextView txtPicName2 = (TextView) convertView.findViewById(R.id.ColMinOderFavourite);
                txtPicName2.setPadding(50, 0, 0, 0);
                txtPicName2.setText(strMin);

                TextView txtPicName3 = (TextView) convertView.findViewById(R.id.ColCostFavourite);
                txtPicName3.setPadding(50, 0, 0, 0);
                txtPicName3.setText(strCost);

            }catch (Exception e) {

            }


           /* // ColImage
            ImageView imageView = (ImageView) convertView.findViewById(R.id.ColImgPathFavourite);
            imageView.getLayoutParams().height = 200;
            imageView.getLayoutParams().width = 200;
            String strPhoto = MyArr.get(position).get("rpicture").toString();


            bitmap=loadBitmap(Server+strPhoto);
            imageView.setImageBitmap(bitmap);


            // ColPicname
            TextView txtPicName = (TextView) convertView.findViewById(R.id.ColNameRFavourite);
            txtPicName.setPadding(50, 0, 0, 0);
            txtPicName.setText( MyArr.get(position).get("rname"));

            TextView txtPicName1 = (TextView) convertView.findViewById(R.id.ColDetailFavourite);
            txtPicName1.setPadding(50, 0, 0, 0);
            txtPicName1.setText(MyArr.get(position).get("detail"));

            TextView txtPicName2 = (TextView) convertView.findViewById(R.id.ColCostFavourite);
            txtPicName2.setPadding(50, 0, 0, 0);
            txtPicName2.setText(MyArr.get(position).get("Cost"));

            TextView txtPicName3 = (TextView) convertView.findViewById(R.id.ColMinOderFavourite);
            txtPicName3.setPadding(50, 0, 0, 0);
            txtPicName3.setText(MyArr.get(position).get("minorder"));*/

            return convertView;

        }

    }

    /***** Get Image Resource from URL (Start) *****/
    private static final String TAG = "ERROR";
    private static final int IO_BUFFER_SIZE = 4 * 1024;
    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }
    /***** Get Image Resource from URL (End) *****/

    public String getJSONUrl(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Download OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download file..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

}
