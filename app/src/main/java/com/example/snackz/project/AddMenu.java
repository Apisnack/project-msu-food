package com.example.snackz.project;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddMenu extends AppCompatActivity {
    String RID;
    String UID;
    private final String TAG = this.getClass().getName();
    ImageView selectphoto;
   // GalleryPhoto galleryPhoto;
   // String strPhoto;
    Button Save ,Cancel;
    String Server=Sever.sever;
    final int GALLERY_REQUEST = 22131;
    Uri imgUri;
    Bitmap bitmap;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Intent intent= getIntent();
        RID = intent.getStringExtra("RID");
        UID = intent.getStringExtra("MemberID");
        System.out.println(RID+"RIDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

        SelectTypeMenu();

        //galleryPhoto = new GalleryPhoto(getApplicationContext());
        selectphoto = (ImageView) findViewById(R.id.IM);
        selectphoto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                openGallery();
            }

        });
        Save = (Button) findViewById(R.id.SaveMenu);
        // Perform action on click
        Save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               SaveData();
                //Specialofmenu();

            }
        });

        // btnCancel
        Cancel = (Button) findViewById(R.id.Cal1Menu);
        // Perform action on click
        Cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(AddMenu.this,manageResterrant.class);
                newActivity.putExtra("MemberID", UID);
                startActivity(newActivity);
            }
        });

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST) {
            imgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(imgUri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                selectphoto.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public void SelectTypeMenu(){

        //System.out.println("XXXXXXXXXXXXXXXXXXXXXX555555555555555555555XXXXXXXXXXXXXXXXXXX");
        final Spinner spin = (Spinner)findViewById(R.id.SpinnerTypeMenu);
        String url = Server+"getTypeMenu.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            JSONArray data = new JSONArray(getHttpPost(url, params));

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);

                map = new HashMap<String, String>();
                map.put("tid", c.getString("tid"));
                map.put("type", c.getString("type"));

                MyArrList.add(map);

            }
            SimpleAdapter sAdap;

            sAdap = new SimpleAdapter(AddMenu.this, MyArrList, R.layout.colum_type,
            new String[] {"type"}, new int[] { R.id.ColTypeMenu});
            spin.setAdapter(sAdap);

            spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    type = MyArrList.get(position).get("type").toString();
                    Toast.makeText(AddMenu.this, "TYPE : "+type, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public boolean SaveData() {
        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtNameM = (EditText) findViewById(R.id.txtNameMenu);
        final EditText txtDetalM = (EditText) findViewById(R.id.txtDetailMenu);
        final EditText txtPriceM = (EditText) findViewById(R.id.txtPriceMenu1);
        final Spinner spinner = (Spinner) findViewById(R.id.SpinnerTypeMenu);
        //final EditText txtTypeM = (EditText) findViewById(R.id.txtTypeMenu);



        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);


        if (txtNameM.getText().length() == 0) {
            ad.setMessage("Please input [ชื่อเมนู] ");
            ad.show();
            txtNameM.requestFocus();
            return false;
        }
        if (txtDetalM.getText().length() == 0) {
            ad.setMessage("Please input [รายละเอียดเมนู] ");
            ad.show();
            txtDetalM.requestFocus();
            return false;
        }


        if (txtPriceM.getText().length() == 0) {
            ad.setMessage("Please input [ราคา] ");
            ad.show();
            txtPriceM.requestFocus();
            return false;
        }


        String url = Server+"AddMenu.php";
        String encodeedImage = ImageBase64.encode(bitmap);
        Log.d(TAG, encodeedImage);


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameM", txtNameM.getText().toString()));
        params.add(new BasicNameValuePair("sDetailM", txtDetalM.getText().toString()));
        params.add(new BasicNameValuePair("sPriceM", txtPriceM.getText().toString()));
        params.add(new BasicNameValuePair("sTypeM", type));
        params.add(new BasicNameValuePair("sImage", encodeedImage));
        params.add(new BasicNameValuePair("sRid", RID.toString()));

        // System.out.print(encodeedImage+"XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        // System.out.println(ImageData+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(AddMenu.this, "AddMenu Successfully", Toast.LENGTH_SHORT).show();
            Intent newActivity = new Intent(AddMenu.this, manageResterrant.class);
            newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity(newActivity);
            txtNameM.setText("");
            txtDetalM.setText("");
            txtPriceM.setText("");
        }

        return true;
    }

    /*public boolean Specialofmenu(){

        final TextView txtP = (TextView)findViewById(R.id.txtP);
        final EditText PriceSe = (EditText)findViewById(R.id.txtPriceMenu2);


        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        if (PriceSe.getText().length() == 0) {
            ad.setMessage("Please input [ราคาพิเศษ] ");
            ad.show();
            PriceSe.requestFocus();
            return false;
        }

        String url = "http://192.168.56.1/LoginPro/AddMenu.php";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("sNameM", txtP.getText().toString()));
        params.add(new BasicNameValuePair("sNameM", PriceSe.getText().toString()));

        String resultServer = getHttpPost(url, params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Unknow Status!";

      /*  JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if (strStatusID.equals("0")) {
            ad.setMessage(strError);
            ad.show();
        } else {
            Toast.makeText(AddMenu.this, "AddMenu Successfully", Toast.LENGTH_SHORT).show();
            PriceSe.setText("");

            Intent intent = new Intent(AddMenu.this, manageResterrant.class);
            intent.putExtra("MemberID", UID);
            startActivity(intent);
        }


        return true;
    }*/


    public String getHttpPost(String url, List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }
}

